package com.graph;

import java.io.Serializable;

/**
 * A link in a linked list of {@link Edge}s.
 */
class EdgeLink implements Serializable {
    /**
     * com.graph.Edge pointer.
     */
    final Edge value;

    /**
     * Pointer to the next Link.
     */
    final EdgeLink next;

    /**
     * Constructs a link holding an com.graph.Edge and pointing to the next one.
     * @param value an com.graph.Edge pointer
     * @param next pointer to the next Link
     */
    EdgeLink(final Edge value, final EdgeLink next) {
        this.value = value;
        this.next = next;
    }
}