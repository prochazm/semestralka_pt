package com.graph;

import java.io.Serializable;

/**
 * Represents an edge in {@link ListGraph}.
 * Works as a messenger.
 */
public class Edge implements Serializable {
    /**
     * The ending node of the edge.
     */
    public final int from;
    public final int to;

    /**
     * Array of the edge costs.
     */
    public final double[] cost;

    /**
     * Constructs an edge to a node with specified costs.
     * @param to target node
     * @param cost array of edge costs
     */
    Edge(final int from, final int to, final double[] cost) {
        this.to = to;
        this.from = from;
        this.cost = cost;
    }

    /**
     * Factory method returns empty
     * edge. Which is basically edge from
     * passed vertex to itself with length
     * equal to zero.
     *
     * @param cyclicId vertex
     * @return "empty" edge
     */
    static Edge empty(final int cyclicId){
        return new Edge(cyclicId, cyclicId, new double[]{0.0, 0.0});
    }

    /**
     * Returns the string representation of the edge for debugging purposes.
     * @return string representation
     */
    @Override
    public String toString() {
        return this.from + " -> " + this.to;
    }
}
