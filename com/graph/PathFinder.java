package com.graph;

import java.util.LinkedList;
import java.util.Objects;

import static com.graph.SolutionType.FAST;
import static com.graph.SolutionType.SHORT;

/**
 * This is implementation of Runnable interface
 * which purpose is to find FW paths on defined
 * slice of graph. This gives user opportunity
 * to successfully parallelize this task if used
 * properly.
 */
class PathFinder implements Runnable {

    private final Edge[][][] out;
    private final int sliceStart;
    private final int sliceEnd;
    private final FloydWarshallSolver solver;
    private final ListGraph graph;
    private final SolutionType type;

    /**
     * Constructor of this runner
     *
     * Takes graph which path finding will be performed
     * on. Target chunk of graph is defined by sliceStart
     * and sliceEnd arguments, to perform path finding on
     * whole graph then zero and sizeof should be passed.
     * {@link FloydWarshallSolver} is needed as source of
     * shortest paths. Solution type defines if time of
     * euclidean distance is prioritized while performing
     * the path finding.
     *
     * @see FloydWarshallSolver
     * @see SolutionType
     *
     * @param graph source graph
     * @param sliceStart start of target chunk
     * @param sliceEnd end of target chunk
     * @param solver solver for the graph
     * @param type type of solution
     */
    PathFinder(final ListGraph graph, final int sliceStart, final int sliceEnd, final FloydWarshallSolver solver, final SolutionType type){
        this.out = type == FAST ? graph.pathsFastest : graph.pathsShortest;
        this.sliceStart = sliceStart;
        this.sliceEnd = sliceEnd;
        this.solver = solver;
        this.graph = graph;
        this.type = type;
    }

    /**
     * Recursively finds path between vertices
     * i and j in graph defined by matrix of
     * distances and matrix of neighbours and
     * returns it as LinkedList
     *
     * @param i start vertex
     * @param j end vertex
     * @param dist distance matrix
     * @param next neighbour matrix
     * @return path between i and j as LinkedList
     */
    private LinkedList<Integer> getPath(final int i, final int j, final double[][] dist, final int[][] next){
        if (dist[i][j] == Double.POSITIVE_INFINITY){
            return null;
        }
        final int bypass = next[i][j];
        if (bypass < 0) {
            return new LinkedList<>();
        }
        final LinkedList<Integer> A = getPath(i, bypass, dist, next);
        final LinkedList<Integer> B = getPath(bypass, j, dist, next);
        if(A == null || B == null){
            return null;
        }
        A.add(bypass);
        A.addAll(B);
        return A;
    }

    /**
     * Run implementation
     *
     * Finds efficient paths in defined slice
     * and outputs it into the graph.
     */
    @Override
    public void run() {
        for (int i = this.sliceStart; i < this.sliceEnd; i++) {
            for (int j = 0; j < this.out.length; j++) {
                if (i == j) {
                    this.out[i][j] = new Edge[]{Edge.empty(i)};
                    continue;
                }
                final LinkedList<Integer> path = Objects.requireNonNull(
                        getPath(i, j, this.solver.getResult(this.type).getA(), this.solver.getResult(this.type).getC()),
                        "a family friendly error message"
                );
                path.addFirst(i);
                path.addLast(j);

                this.out[i][j] = new Edge[path.size() - 1];
                for (int k = 0; k < path.size() - 1; k++) {
                    this.out[i][j][k] = this.graph.getEdge(path.get(k), path.get(k + 1));
                }
            }
        }

    }
}
