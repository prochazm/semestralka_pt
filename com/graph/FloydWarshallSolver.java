package com.graph;

import com.misc.Triple;

import static com.main.Main.statusMessage;

/**
 * Solves FW using multiple threads.
 */
class FloydWarshallSolver {

    private final ListGraph graph;
    private Triple<double[][], double[][], int[][]> result0;
    private Triple<double[][], double[][], int[][]> result1;

    /**
     * Private constructor, use {@link FloydWarshallSolver#get(ListGraph)}
     *
     * @param graph graph which will Floyd-Warshall be performed on
     */
    private FloydWarshallSolver(final ListGraph graph){
        this.graph = graph;
    }

    /**
     * Returns a new instance of FWS around a target ListGraph.
     * @param graph target ListGraph
     * @return a new instance of FWS
     */
    static FloydWarshallSolver get(final ListGraph graph){
        return new FloydWarshallSolver(graph);
    }

    /**
     * Returns the target result of FW.
     * @param type 0 for shortest paths, 1 for fastest paths
     * @return a triplet of FW matrices
     */
    Triple<double[][], double[][], int[][]> getResult(final SolutionType type){
        return (type == SolutionType.FAST) ? this.result0 : this.result1;
    }

    /**
     * Solves the FW problem.
     * @param type 0 for shortest paths, 1 for fastest paths
     * @return FWS pointer for further solving
     */
    FloydWarshallSolver solve(final SolutionType type) {
        final int V = this.graph.size();
        final double[][][] dist = new double[2][][];
        dist[0] = new double[V][V];
        dist[1] = new double[V][V];
        final int[][] next = new int[V][V];
        int i, j, k;

        for(i = 0; i < V; i++){
            for(j = 0; j < V; j++){
                next[i][j] = -1;
                dist[0][i][j] = Double.POSITIVE_INFINITY;
                dist[1][i][j] = Double.POSITIVE_INFINITY;
            }
            dist[0][i][i] = 0.0;
            dist[1][i][i] = 0.0;
        }

        this.graph.forEachEdge(edge -> {
            dist[0][edge.from][edge.to] = edge.cost[0];
            dist[1][edge.from][edge.to] = edge.cost[1];
        });

        /* Add all vertices one by one to the set of intermediate
           vertices.
          ---> Before start of an iteration, we have shortest
               distances between all pairs of vertices such that
               the shortest distances consider only the vertices in
               set {0, 1, 2, .. k-1} as intermediate vertices.
          ----> After the end of an iteration, vertex no. k is added
                to the set of intermediate vertices and the set
                becomes {0, 1, 2, .. k} */
        statusMessage("Floyd-Warshall of type " + type + " initialized");
        for (k = 0; k < V; k++)
        {
            if(k % (V / 10) == 0) {
                statusMessage("progress = " + (double)(100*k)/V + "%");
            }
            // Pick all vertices as source one by one
            for (i = 0; i < V; i++)
            {
                // Pick all vertices as destination for the
                // above picked source
                for (j = 0; j < V; j++)
                {
                    // If vertex k is on the shortest path from
                    // i to j, then update the value of dist[i][j]
                    if (dist[type.get()][i][k] + dist[type.get()][k][j] < dist[type.get()][i][j]) {
                        dist[0][i][j] = dist[0][i][k] + dist[0][k][j];
                        dist[1][i][j] = dist[1][i][k] + dist[1][k][j];
                        next[i][j] = k;
                    }
                }
            }
        }

        final Triple<double[][], double[][], int[][]> result = new Triple<>(dist[0], dist[1], next);
        if (type == SolutionType.FAST){
            this.result0 = result;
        } else {
            this.result1 = result;
        }
        return this;
    }


}
