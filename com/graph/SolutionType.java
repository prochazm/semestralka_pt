package com.graph;

/**
 * Represents a solution type for the FW solver, namely whether to target fastest or shortest paths.
 */
public enum SolutionType {
    SHORT(0), FAST(1);

    private final int value;

    SolutionType(final int value){
        this.value = value;
    }

    int get(){
        return this.value;
    }
}