package com.graph;

import java.io.Serializable;
import java.util.function.Consumer;

/**
 * Implements an undirected list-of-neighbors com.graph (V, E):
 * for each vertex vi of V, there is a linked list Li of edges e such that e lies in Li iff e.start = vi
 */
@SuppressWarnings("unused")
public class ListGraph implements Serializable {
    /**
     * Default capacity of a com.graph.
     */
    private static final int DEFAULT_CAPACITY = 2000;
    public double[][] FWMatrixFastestDist;
    public double[][] FWMatrixFastestTime;
    double[][] FWMatrixShortestDist;
    double[][] FWMatrixShortestTime;
    Edge[][][] pathsFastest;
    Edge[][][] pathsShortest;

    /**
     * Size of V.
     */
    private final int capacity;

    private final int costCount;

    /**
     * Linked lists Li.
     */
    private final EdgeLink[] nodes;

    /**
     * Constructs a com.graph.ListGraph of specified |V|.
     * @param capacity size of V
     */
    ListGraph(final int capacity, final int costCount){
        this.capacity = capacity;
        this.nodes = new EdgeLink[this.capacity];
        this.costCount = costCount;
    }

    /**
     * Constructs a com.graph.ListGraph with default capacity.
     */
    public ListGraph(final int costCount){
        this(DEFAULT_CAPACITY, costCount);
    }

    /**
     * Implements the operation of adding an edge.
     * The edge is undirected by default.
     * @param a source
     * @param b target
     * @param cost array of different costs of the edge
     */
    void addEdge(final int a, final int b, final double[] cost) {
        if(cost.length != this.costCount){
            throw new IllegalArgumentException("The cost array is not the same size as expected.");
        }

        this.nodes[a] = new EdgeLink(new Edge(a, b, cost), this.nodes[a]);
        this.nodes[b] = new EdgeLink(new Edge(b, a, cost), this.nodes[b]);
    }

    /**
     * Returns the shortest possible path between a and b.
     * @param a a
     * @param b b
     * @return the shortest possible path
     */
    public Edge[] shortestPath(final int a, final int b){
        return this.pathsShortest[a][b];
    }

    /**
     * Returns the fastest possible path between a and b.
     * @param a a
     * @param b b
     * @return the fastest possible path
     */
    public Edge[] fastestPath(final int a, final int b){
        return this.pathsFastest[a][b];
    }

    /**
     * Returns the length of the shortest possible path between a and b.
     * @param a a
     * @param b b
     * @return the shortest possible path length
     */
    public double shortestPathDist(final int a, final int b){
        return this.FWMatrixShortestDist[a][b];
    }

    /**
     * Returns the time consumption of the shortest possible path between a and b.
     * @param a a
     * @param b b
     * @return the shortest possible path time consumption
     */
    public double shortestPathTime(final int a, final int b){
        return this.FWMatrixShortestTime[a][b];
    }

    /**
     * Returns the length of the fastest possible path between a and b.
     * @param a a
     * @param b b
     * @return the fastest possible path length
     */
    public double fastestPathDist(final int a, final int b){
        return this.FWMatrixFastestDist[a][b];
    }

    /**
     * Returns the time consumption of the fastest possible path between a and b.
     * @param a a
     * @param b b
     * @return the fastest possible path time consumption
     */
    public double fastestPathTime(final int a, final int b){
        return this.FWMatrixFastestTime[a][b];
    }

    /**
     * The amount of vertices of the graph.
     * @return the amount of vertices of the graph.
     */
    public int size() {
        return this.capacity;
    }

    /**
     * Finds a target edge.
     * @param a edge start
     * @param b edge end
     * @return target edge pointer
     */
    Edge getEdge(final int a, final int b) {
        EdgeLink current = this.nodes[a];
        while(current != null){
            if(current.value.to == b) {
                return current.value;
            }
            current = current.next;
        }
        return null;
    }

    /**
     * Performs an action for each edge of the graph.
     * @param action target action
     */
    void forEachEdge(final Consumer<? super Edge> action){
        for (final EdgeLink node : this.nodes) {
            EdgeLink current = node;
            while (current != null) {
                action.accept(current.value);
                current = current.next;
            }
        }
    }
}
