package com.graph;

import com.misc.Pair;
import com.world.Settlement;

import java.io.*;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static com.graph.SolutionType.FAST;
import static com.graph.SolutionType.SHORT;
import static com.main.Main.COMMON_RANDOM;
import static com.main.Main.statusMessage;

/**
 * Generates graphs and handles their file I/O.
 */
public class GraphGenerator {
    private static final Random r = COMMON_RANDOM;

    /**
     * Generates a graph representing a country map. The technique used generates a square grid of MST agglomerates
     * (all cities are connected so that the least amount of road is used), which have their respective central points
     * connected by a freeway and their respective extreme points connected by a connector road.
     * @param width grid width
     * @param height grid height
     * @param block_size size of a single square edge in km
     * @param count amount of cities in the country
     * @return the country map + all cities in an array
     */
    public static Pair<ListGraph, Settlement[]> byGrid(final int width, final int height, final double block_size, final int count){
        final Settlement[] settlements = new Settlement[count];

        final boolean[][] edgeFromTo = new boolean[count][count];
        final ListGraph graph = new ListGraph(count, 2);

        final double[] x = new double[count];
        final double[] y = new double[count];

        final int[] centralPoint = new int[width * height];

        final int[] eastestPoint = new int[width * height];
        final int[] westestPoint = new int[width * height];
        final int[] northestPoint = new int[width * height];
        final int[] southestPoint = new int[width * height];

        final HashMap<Integer, ArrayList<Integer>> map = new HashMap<>();

        generateRandomPoints(count, block_size, width, height, x, y, settlements, map);

        map.forEach((id, list) -> {                                 // for each grid square,
            final int current_x = id % width;                             //
            final int current_y = id / width;                             // left corner coords

            final double center_x = current_x*block_size + block_size/2;
            final double center_y = current_y*block_size + block_size/2;

            double smallest_dist = dist(x[list.get(0)], y[list.get(0)], center_x, center_y);
            centralPoint[id] = list.get(0);
            eastestPoint[id] = list.get(0);
            westestPoint[id] = list.get(0);
            northestPoint[id] = list.get(0);
            southestPoint[id] = list.get(0);

            for(int i = 1; i < list.size(); i++){                    // for each point in the grid square, find extremes
                final int pt = list.get(i);
                final double new_dist = dist(x[pt], y[pt], center_x, center_y);
                if(new_dist < smallest_dist){
                    smallest_dist = new_dist;
                    centralPoint[id] = pt;
                }

                if(x[pt] < x[westestPoint[id]]){
                    westestPoint[id] = pt;
                }
                else if(x[pt] > x[eastestPoint[id]]) {
                    eastestPoint[id] = pt;
                }

                if(y[pt] < y[northestPoint[id]]){
                    northestPoint[id] = pt;
                }
                else if(y[pt] > y[southestPoint[id]]){
                    southestPoint[id] = pt;
                }
            }
        });

        final int[] right_down = {                                       // directions in which two agglomerates are connected
                            +1,
                width
        };

        map.forEach((id, list) -> {                                 // for each grid square,
            final int list_size = list.size();
            if(list_size == 0) {
                return;                              // if it contains no points, leave
            }

            if(list_size > 1) {                                     // if it contains more then 1 point,
                final ArrayList<Edge> edges = new ArrayList<>();

                for (int i = 0; i < list_size - 1; i++) {
                    final int a = list.get(i);

                    for (int j = i + 1; j < list_size; j++) {
                        final int b = list.get(j);

                        final double dist = dist(x[a], y[a], x[b], y[b]);
                        final double time = collectorTime(dist);

                        final double[] costs = {dist, time};

                        edges.add(new Edge(i, j, costs));
                    }
                }

                final List<Edge> MST = KruskalMST(edges, list_size);     // find MST
                MST.forEach(edge -> {
                    final int to = list.get(edge.to);
                    final int from = list.get(edge.from);
                    graph.addEdge(from, to, edge.cost);
                    edgeFromTo[from][to] = true;
                    edgeFromTo[to][from] = true;
                });

                for (int k = 0; k < 5; k++) {                           // add 5 random roads
                    final int r = GraphGenerator.r.nextInt(edges.size());
                    final int from = list.get(edges.get(r).from);
                    final int to = list.get(edges.get(r).to);
                    if (!edgeFromTo[from][to]) {
                        graph.addEdge(from, to, edges.get(r).cost);
                        edgeFromTo[from][to] = true;
                        edgeFromTo[to][from] = true;
                    }
                }
            }

            final int chosen_src = centralPoint[id];

            for (final int i : right_down) {                                  // in connecting directions,
                final int target_pos = id + i;
                if(target_pos >= 0 && target_pos <= width*height){
                    if((i == +1 && target_pos / width != id / width) || (i == width + 1 && target_pos / width != id / width + 1)){
                        continue;
                    }

                    final ArrayList<Integer> target_block = map.get(target_pos);
                    if(target_block == null || target_block.isEmpty()){
                        continue;
                    }

                    final int from;
                    final int to;
                    if(i == 1){
                        from = eastestPoint[id];
                        to = westestPoint[target_pos];
                    }else{
                        from = southestPoint[id];
                        to = northestPoint[target_pos];
                    }

                    addEdgeUtil(graph, edgeFromTo, from, to, x, y);      // connect related extreme points between
                                                                         // agglomerates

                    final int chosen_dst = centralPoint[target_pos];

                    final double dist = Math.sqrt(
                            (x[chosen_src] - x[chosen_dst])*(x[chosen_src] - x[chosen_dst]) +
                            (y[chosen_src] - y[chosen_dst])*(y[chosen_src] - y[chosen_dst])
                    );
                    final double time = freewayTime(dist);                    // connect central points between agglomerates

                    final double[] costs = {dist, time};

                    if(!edgeFromTo[chosen_src][chosen_dst]) {
                        graph.addEdge(chosen_src, chosen_dst, costs);
                        edgeFromTo[chosen_src][chosen_dst] = true;
                        edgeFromTo[chosen_dst][chosen_src] = true;
                    }
                }
            }
        });

        final FloydWarshallSolver fwSolver = FloydWarshallSolver.get(graph).solve(FAST).solve(SHORT);
        graph.pathsFastest =  new Edge[count][count][];
        graph.pathsShortest = new Edge[count][count][];

        final int threadCount = 2;
        final ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(threadCount);
        statusMessage("finding shortest and fastest paths initialized using " + threadCount + "threads");

        final int increment = count / threadCount + 1;
        for (int i = 0; i < count; i += increment){
            executor.execute(new PathFinder(graph, i, Integer.min(i + increment, count), fwSolver, SHORT));
            executor.execute(new PathFinder(graph, i, Integer.min(i + increment, count), fwSolver, FAST));
        }
        executor.shutdown();
        try {
            if (!executor.awaitTermination(Integer.MAX_VALUE, TimeUnit.MILLISECONDS)){
                throw new RuntimeException("Executor didn't terminate");
            }
        } catch (final InterruptedException e) {
            e.printStackTrace();
        }


        graph.FWMatrixFastestDist = fwSolver.getResult(FAST).getA();
        graph.FWMatrixShortestDist = fwSolver.getResult(SHORT).getA();
        graph.FWMatrixFastestTime = fwSolver.getResult(FAST).getB();
        graph.FWMatrixShortestTime = fwSolver.getResult(SHORT).getB();

        statusMessage("generation complete");

        return new Pair<>(graph, settlements);
    }

    private static void generateRandomPoints(final int count, final double block_size, final int width, final double height, final double[] x, final double[] y, final Settlement[] settlements, final Map<Integer, ArrayList<Integer>> map) {
        for(int i = 0; i < count; i++){                             // generate random points
            final double cur_x = r.nextDouble()*block_size*width;
            final double cur_y = r.nextDouble()*block_size*height;
            x[i] = cur_x;
            y[i] = cur_y;

            final int gridX = (int) (cur_x / block_size);
            final int gridY = (int) (cur_y / block_size);                 // put them in the grid

            settlements[i] = new Settlement(cur_x, cur_y, gridX, gridY, i);

            final int id = gridX + width * gridY;

            map.computeIfAbsent(id, k -> new ArrayList<>());
            map.get(id).add(i);
        }
    }

    private static void addEdgeUtil(final ListGraph graph, final boolean[][] edgeFromTo, final int from, final int to, final double[] x, final double[] y) {
        final double dist = dist(x[from], y[from], x[to], y[to]);
        final double time = dist * (0.5 + gaussianRange(0.2));

        final double[] costs = {dist, time};

        graph.addEdge(from, to, costs);
        edgeFromTo[from][to] = true;
        edgeFromTo[to][from] = true;
    }

    /**
     * Exports data describing a country to a file.
     * @param data country data
     * @param filename target filename
     */
    public static void exportToFile(final Pair<ListGraph, Settlement[]> data, final String filename){
        try (final ObjectOutputStream out = new ObjectOutputStream(
                new BufferedOutputStream(
                new FileOutputStream(filename)))){
            out.writeObject(data);
        } catch (final IOException i) {
            i.printStackTrace();
        }
    }

    /**
     * Loads data describing a country from a file.
     * @param filename target filename
     * @return country data
     */
    @SuppressWarnings("unchecked")
    public static Pair<ListGraph, Settlement[]> importFromFile(final String filename){
        final Pair<ListGraph, Settlement[]> data;
        try (final ObjectInputStream in = new ObjectInputStream(
                new BufferedInputStream(
                new FileInputStream(filename)))){
            data = (Pair<ListGraph, Settlement[]>) in.readObject();
        } catch (final IOException | ClassNotFoundException i) {
            return null;
        }
        return data;
    }

    /**
     * Returns random real number with probability
     * given by gaussian distribution with given
     * range which is symmetrical around zero.
     *
     * @param range one of range boundaries
     * @return random number in range
     */
    private static double gaussianRange(final double range){
        final double gen = r.nextGaussian() * range;
        if(gen < -range) {
            return -range;
        }
        else if(gen > range) {
            return range;
        }
        return gen;
    }

    private static class Subset {
        int parent, rank;
    }

    /**
     * Recursively finds root of subset
     * defined by passed item contained
     * in it.
     *
     * @param subsets subsets
     * @param i item in subset
     * @return root
     */
    private static int find(final Subset[] subsets, final int i){
        if (subsets[i].parent != i) {
            subsets[i].parent = find(subsets, subsets[i].parent);
        }
        return subsets[i].parent;
    }

    /**
     * Joins two subsets defined by items
     * contained in them.
     *
     * @param subsets subsets
     * @param x item in one subset
     * @param y item in other subset
     */
    private static void Union(final Subset[] subsets, final int x, final int y){
        final int xroot = find(subsets, x);
        final int yroot = find(subsets, y);
        if (subsets[xroot].rank < subsets[yroot].rank) {
            subsets[xroot].parent = yroot;
        }
        else if (subsets[xroot].rank > subsets[yroot].rank) {
            subsets[yroot].parent = xroot;
        }
        else{
            subsets[yroot].parent = xroot;
            subsets[xroot].rank++;
        }
    }

    /**
     * Calculates minimal spanning tree using Kruskals
     * algorithm.
     *
     * @param input initial graph as list of Edge instances
     * @param V Vertices count
     * @return MST as list of Edge instances
     */
    private static List<Edge> KruskalMST(final List<Edge> input, final int V){
        final ArrayList<Edge> ret = new ArrayList<>();
        int i;
        final Edge[] edge = input.toArray(new Edge[0]);
        Arrays.sort(edge, Comparator.comparingDouble(awd -> awd.cost[0]));
        final Subset[] subsets = new Subset[V];
        for(i=0; i<V; ++i){
            subsets[i]=new Subset();
            subsets[i].parent = i;
            subsets[i].rank = 0;
        }
        i = 0;
        while (ret.size() < V - 1){
            final Edge next_edge;
            next_edge = edge[i++];

            final int x = find(subsets, next_edge.from);
            final int y = find(subsets, next_edge.to);

            if (x != y){
                ret.add(next_edge);
                Union(subsets, x, y);
            }
        }
        return ret;
    }

    /**
     * Calculates euclidean distance of two
     * points in bi-dimensional space.
     *
     * @param X1 x coordinate of one point
     * @param Y1 y coordinate of one point
     * @param X2 x coordinate of other point
     * @param Y2 y coordinate of other point
     * @return euclidean distance of two points
     */
    private static double dist(final double X1, final double Y1, final double X2, final double Y2){
        return Math.sqrt((X1 - X2) * (X1 - X2) + (Y1 - Y2) * (Y1 - Y2));
    }

    /**
     * Calculates time needed to cross
     * freeway of given length randomized
     * by gaussian distribution.
     *
     * @param dist distance needed to be crossed
     * @return time needed for actual crossing
     */
    private static double freewayTime(final double dist){ // = dalnice
        return dist * (0.5 + gaussianRange(0.3)) * 60;
    }

    /**
     * Calculates time needed to cross
     * a road of given length randomized
     * by gaussian distribution.
     *
     * @param dist distance needed to be crossed
     * @return time needed for actual crossing.
     */
    private static double collectorTime(final double dist){ // = silnice
        return dist * (1.0 + gaussianRange(0.3)) * 60;
    }
}
