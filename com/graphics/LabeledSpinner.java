package com.graphics;

import javax.swing.*;
import javax.swing.event.ChangeListener;
import java.awt.*;

/**
 * A spinner which also has a label attached to it.
 */
public class LabeledSpinner extends JPanel {
    private static final Dimension SIZE = new Dimension(400, 40);
    private static final Dimension SPINNER_SIZE = new Dimension(70, 20);
    private final SpinnerNumberModel model;

    private JSpinner spinner;

    /**
     * Integer spinner.
     * @param current default value
     * @param min minimal value
     * @param max maximal value
     * @param step step size
     * @param label label text
     */
    public LabeledSpinner(final int current, final int min, final int max, final int step, final String label) {
        super();
        this.model = new SpinnerNumberModel(current, min, max, step);
        stuff(label);
    }

    /**
     * Double spinner.
     * @param current default value
     * @param min minimal value
     * @param max maximal value
     * @param step step size
     * @param label label text
     */
    public LabeledSpinner(final double current, final double min, final double max, final double step, final String label) {
        super();
        this.model = new SpinnerNumberModel(current, min, max, step);
        stuff(label);
    }

    private void stuff(final String label) {
        this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        final JLabel label1 = new JLabel(label);
        this.spinner = new JSpinner(this.model);
        this.spinner.setMinimumSize(SPINNER_SIZE);
        this.spinner.setMaximumSize(SPINNER_SIZE);
        this.spinner.setSize(SPINNER_SIZE);
        this.spinner.setPreferredSize(SPINNER_SIZE);

        label1.setAlignmentX(0.0f);
        this.spinner.setAlignmentX(1.0f);

        this.add(label1);
        this.add(Box.createHorizontalGlue());
        this.add(this.spinner);

        this.setMaximumSize(SIZE);

        this.spinner.setEnabled(false);
        label1.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 13));
    }

    /**
     * Enables adjusting of the spinner value.
     */
    public void setEnabled() {
        this.spinner.setEnabled(true);
    }

    /**
     * Disables adjusting of the spinner value.
     */
    public void setDisabled() {
        this.spinner.setEnabled(false);
    }

    /**
     * Tests the spinner's state.
     * @return spinner state
     */
    @Override
    public boolean isEnabled() {
        return this.spinner.isEnabled();
    }

    /**
     * Adds a change listener to the spinner.
     * @param listener change listener
     */
    public void addChangeListener(final ChangeListener listener){
        this.spinner.addChangeListener(listener);
    }

    /**
     * @return the spinner's value.
     */
    public Object getValue(){
        return this.spinner.getValue();
    }
}
