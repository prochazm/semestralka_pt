package com.misc;

import com.world.Order;

import java.util.ArrayList;

/**
 * Represents an arraylist of orders. Its sole purpose is to eliminate a 'generic array creation' error.
 */
public class ListOfOrders extends ArrayList<Order> {
}
