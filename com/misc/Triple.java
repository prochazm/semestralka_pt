package com.misc;

import java.io.Serializable;

/**
 * Represents a triplet of variables.
 * @param <A> First variable.
 * @param <B> Second variable.
 * @param <C> Third variable.
 */
public class Triple<A, B, C> implements Serializable {
    private final A a;
    private final B b;
    private final C c;

    /**
     * Creates a triplet of variables.
     * @param a First variable.
     * @param b Second variable.
     * @param c Third variable.
     */
    public Triple(final A a, final B b, final C c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    /**
     * Returns the first variable.
     * @return the first variable.
     */
    public A getA() {
        return this.a;
    }

    /**
     * Returns the second variable.
     * @return the second variable.
     */
    public B getB() {
        return this.b;
    }

    /**
     * Returns the third variable.
     * @return the third variable.
     */
    public C getC() {
        return this.c;
    }
}
