package com.misc;

/**
 * Represents a function that maps long -> long. Its sole purpose is to eliminate a 'generic array creation' error.
 */
public interface LLFunction {
    long apply(long n);
}
