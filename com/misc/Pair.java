package com.misc;

import java.io.Serializable;

/**
 * Represents a pair of variables.
 * @param <A> First variable.
 * @param <B> Second variable.
 */
public class Pair<A, B> implements Serializable {
    private final A a;
    private final B b;

    /**
     * Creates a pair of variables.
     * @param a First variable.
     * @param b Second variable.
     */
    public Pair(final A a, final B b) {
        this.a = a;
        this.b = b;
    }

    /**
     * Returns the first variable.
     * @return the first variable.
     */
    public A getA() {
        return this.a;
    }

    /**
     * Returns the second variable.
     * @return the second variable.
     */
    public B getB() {
        return this.b;
    }
}
