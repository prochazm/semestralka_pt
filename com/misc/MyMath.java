package com.misc;

/**
 * A bundle of custom auxiliary math functions.
 */
public class MyMath {
    /**
     * Let a and b be floating point 64-bit numbers, we say that a == b iff this function returns true. Does not preserve
     * transitivity.
     * @param a a
     * @param b b
     * @return true if a == b, else false
     */
    public static boolean doubleEquals(final double a, final double b) {
        return Math.abs(a - b) < 0.00001;
    }
}
