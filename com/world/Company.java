package com.world;

import com.graph.Edge;
import com.graph.ListGraph;
import com.main.Main;
import com.misc.ListOfOrders;
import com.misc.Pair;
import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.cplex.IloCplex;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

import static com.main.Main.*;
import static com.misc.MyMath.doubleEquals;

/**
 * Represents a company selling palettes to customers.
 */
public class Company {
    /**
     * The soonest time a transporter can depart from depot.
     */
    private static final int DELIVERY_START_TIME = Time.toSeconds(6, 0);

    /**
     * The cost of one kilometer of transporter travel in price units.
     */
    public static double _cpkmt = 25;

    /**
     * The amount of price units a single palette is sold for.
     */
    public static double _cppd = 5000;

    /**
     * The amount of price units a single driver is paid per second of work.
     */
    public static double _cpst = 0.02;

    /**
     * The upper exclusive bound of cluster size the optimization call is going to be called upon. The larger the cluster
     * size, the better solutions but drastically longer computation time.
     */
    public static int _cluster_size_bound = 8;

    /**
     * The time limit after which the optimization is terminated and the best solution is returned. The larger
     * the time limet the better solutions.
     */
    public static int _cplex_timeout = 10;

    private static final String IDLE_STATE = "IDLE";
    private static final String ACCEPTING_STATE = "ACCEPTING DELIVERY";

    private final int company_location;

    private final ListGraph world_map;

    private final List<Transporter> departed_transporters;
    private final PriorityQueue<Transporter> assigned_transporters;
    private final ListOfOrders orders;
    private final ListOfOrders[][] orderGrid;

    private final int[] settlement_transporters;
    private final int[] settlement_orderAmounts;

    private int recalculation_time;
    private int complete_orders = 0;

    private final int[] delivered_palettes_in_day;
    private final int[] km_travelled_in_day;
    private final double[] profit_in_day;
    private final int[][] palettes_delivered_by_transporter_in_day;
    private final int[][] km_travelled_by_transporter_in_day;
    private final int[][] s_travelled_by_transporter_in_day;
    private final int[] palettes_delivered_by_transporter_total;
    private final int[] km_travelled_by_transporter_total;
    private final int[] s_travelled_by_transporter_total;

    /**
     * Founds a company in one of the country's cities.
     * @param data country data
     */
    public Company(final Pair<ListGraph, Settlement[]> data) {
        final int dayCount = Main.INIT_ORDERS.length;

        this.delivered_palettes_in_day = new int[dayCount];
        this.km_travelled_in_day = new int[dayCount];
        this.profit_in_day = new double[dayCount];
        this.palettes_delivered_by_transporter_in_day = new int[2000][dayCount];
        this.km_travelled_by_transporter_in_day = new int[2000][dayCount];
        this.s_travelled_by_transporter_in_day = new int[2000][dayCount];
        this.palettes_delivered_by_transporter_total = new int[2000];
        this.km_travelled_by_transporter_total = new int[2000];
        this.s_travelled_by_transporter_total = new int[2000];

        this.world_map = data.getA();

        this.company_location = COMMON_RANDOM.nextInt(data.getB().length);

        this.assigned_transporters = new PriorityQueue<>(300, (o1, o2) -> {
            if (o1.getDepartureTime() == o2.getDepartureTime()) {
                return o1.getId() - o2.getId();
            } else{
                return o1.getDepartureTime() - o2.getDepartureTime();
            }
        });

        this.departed_transporters = new ArrayList<>();
        this.orders = new ListOfOrders();
        this.orderGrid = new ListOfOrders[GRID_WIDTH][GRID_HEIGHT];
        for(int i = 0; i < GRID_WIDTH; i++){
            for(int j = 0; j < GRID_HEIGHT; j++){
                this.orderGrid[i][j] = new ListOfOrders();
            }
        }
        this.settlement_transporters = new int[data.getB().length];
        this.settlement_orderAmounts = new int[data.getB().length];
    }

    /**
     * Departs all transporters that should be departed.
     * @return the newly departed transporters
     */
    @SuppressWarnings("ConstantConditions")
    public List<Transporter> departTransporters() {
        final int current_time = Time.current_time();
        final ArrayList<Transporter> departedNow = new ArrayList<>();

        while(!this.assigned_transporters.isEmpty() && this.assigned_transporters.peek().getDepartureTime() <= current_time){
            final Transporter departed = this.assigned_transporters.poll();

            departed.move(current_time - departed.getDepartureTime());
            this.departed_transporters.add(departed);

            departedNow.add(departed);
        }
        return departedNow;
    }

    /**
     * Moves all transporters in time by some Δt.
     * @param deltaT time shift
     * @return all transporters returned to depot by the current moveTransporters(Δt) call
     */
    public List<Transporter> moveTransporters(final long deltaT) {
        final ArrayList<Transporter> arrivedNow = new ArrayList<>();

        for(final Iterator i = this.departed_transporters.iterator(); i.hasNext(); ){
            final Transporter transporter = (Transporter) i.next();
            if(transporter.move(deltaT)){
                arrivedNow.add(transporter);
                i.remove();
            }
        }

        arrivedNow.forEach(arrivedTransporter -> Transporter.restoreIndex(arrivedTransporter.getId()));
        return arrivedNow;
    }

    /**
     * @return Amount of currently departed transporters.
     */
    public String dtCount() {
        return String.valueOf(this.departed_transporters.size());
    }

    /**
     * @return Amount of transporters currently waiting for departure.
     */
    public String atCount() {
        return String.valueOf(this.assigned_transporters.size());
    }

    /**
     * @return The company's location on the country map.
     */
    public int getCompany_location() {
        return this.company_location;
    }

    /**
     * Tests whether a customer order is reachable in time from a target location.
     * @param location target location
     * @param soonest_departure the time of soonest departure from target location
     * @param order target order
     * @return amount of extra time a potential transporter would have were it to fulfill the order
     */
    private int establishRoute(final int location, final int soonest_departure, final Order order){
        return order.getAdEnd() - soonest_departure - (int) this.world_map.fastestPathTime(location, order.getLocation());
    }

    /**
     * Optimizes the transporter paths to all current orders so as to achieve the maximal profit.
     */
    public void solveOrders(){
        medianCut(0, 0, GRID_WIDTH-1, GRID_HEIGHT-1, this.orders.size());
        this.orders.clear();
        for(int i = 0; i < GRID_WIDTH; i++){
            for(int j = 0; j < 10; j++){
                this.orderGrid[i][j].clear();
            }
        }
        resetRCTime();
        this.complete_orders = 0;
    }

    /**
     * Splits the grid plane (an integer rectangle) of order locations by medians into clusters which all have size less than
     * {@link Company#_cluster_size_bound} and calls a LP optimization procedure on each of these clusters.
     * @param X1 plane left bound
     * @param Y1 plane upper bound
     * @param X2 plane right bound
     * @param Y2 plane lower bound
     * @param orders_size amount of orders in the plane
     */
    private void medianCut(final int X1, final int Y1, final int X2, final int Y2, final int orders_size) {
        if(orders_size == 0) {
            return;
        }
        if(orders_size < _cluster_size_bound){           // if the amount of orders here is finally less than CSB, call LP proc.
            final ListOfOrders cluster = new ListOfOrders();
            for(int i = X1; i <= X2; i++){
                for(int j = Y1; j <= Y2; j++){
                    cluster.addAll(this.orderGrid[i][j]);
                }
            }
            solveOrdersSub(cluster, Time.current_time() < DELIVERY_START_TIME ? DELIVERY_START_TIME : Time.current_time());
            this.complete_orders += cluster.size();
            statusMessage("Calculating the best routes for transporters to maximize profit. (done " + this.complete_orders + "/" + this.orders.size() + ")");
            return;
        }

        if(X1 == X2 && Y1 == Y2){ //no more median-cutting available, possibly more points than cluster bound,
            final ListOfOrders cluster = new ListOfOrders();
            final int size = this.orderGrid[X1][Y1].size();
            for (int i = 0; i < size; i++) {
                cluster.add(this.orderGrid[X1][Y1].get(i));
                if((i + 1) % (_cluster_size_bound - 1) == 0){        // therefore split into appropriate parts.
                    solveOrdersSub(cluster, Time.current_time() < DELIVERY_START_TIME ? DELIVERY_START_TIME : Time.current_time());
                    this.complete_orders += cluster.size();
                    statusMessage("Calculating the best routes for transporters to maximize profit. (done " + this.complete_orders + "/" + this.orders.size() + ")");
                    cluster.clear();
                }
            }
            return;
        }

        int cutting_index = 0;
        int orders_to_the_left = 0;
        if(X2 - X1 > Y2 - Y1){                                      // always split by larger side
            while(orders_to_the_left < orders_size/2) {             // until median is reached
                if(X1 + cutting_index == X2){
                    break;
                }
                for (int y = Y1; y <= Y2; y++) {
                    orders_to_the_left += this.orderGrid[X1 + cutting_index][y].size();
                }
                cutting_index++;                                    // move the cutting index to the right
            }

            medianCut(X1, Y1, X1 + cutting_index - 1, Y2, orders_to_the_left);  // sub call on both halves
            medianCut(X1 + cutting_index, Y1, X2, Y2, orders_size - orders_to_the_left);
        }else{
            while(orders_to_the_left < orders_size/2) {
                if(Y1 + cutting_index == Y2){
                    break;
                }
                for (int x = X1; x <= X2; x++) {
                    orders_to_the_left += this.orderGrid[x][Y1 + cutting_index].size();
                }
                cutting_index++;
            }

            medianCut(X1, Y1, X2, Y1 + cutting_index -1 , orders_to_the_left);
            medianCut(X1, Y1 + cutting_index, X2, Y2, orders_size - orders_to_the_left);
        }
    }

    private void solveOrdersSub(final ListOfOrders orders, final int departure_time){
        final int v = orders.size();
        final int[] transporters = new int[v];
        for (int i = 0; i < v; i++) {
            transporters[i] = i;
        }

        final int CustomersNumber = orders.size();

        final int[] Customers = new int[CustomersNumber];
        for (int i = 0; i < Customers.length; i++) {
            Customers[i] = i + 1;
        }

        final int[] CustomersAndDepots = new int[CustomersNumber + 2];
        for (int i = 0; i < CustomersAndDepots.length; i++) {
            CustomersAndDepots[i] = i;
        }

        final int Capacity = Transporter.MAXIMUM_CAPACITY;

        final int[] L2Gid = new int[v + 2];                       // maps local id to global id of a settlement (location)
        L2Gid[0] = this.company_location;
        L2Gid[v + 1] = this.company_location;
        for(final int i : Customers){
            L2Gid[i] = orders.get(i - 1).getLocation();
        }

        final int[] Demand = new int[v + 2];
        Demand[0] = 0;
        Demand[v + 1] = 0;
        for(final int i : Customers){
            Demand[i] = orders.get(i - 1).getSize();
        }

        final int[] LBTW = new int[v + 2];
        LBTW[0] = 0;
        LBTW[v + 1] = 0;
        for(final int i : Customers){
            LBTW[i] = orders.get(i - 1).getAdStart() - departure_time;
        }

        final int[] UBTW = new int[v + 2];
        UBTW[0] = Time.DAY_LENGTH;
        UBTW[v + 1] = Time.DAY_LENGTH;
        for(final int i : Customers){
            UBTW[i] = orders.get(i - 1).getAdEnd() - departure_time;
        }

        final double[][] dist = new double[CustomersAndDepots.length][CustomersAndDepots.length];
        final double[][] time = new double[CustomersAndDepots.length][CustomersAndDepots.length];

        try {
            final IloCplex cplex = new IloCplex();
            cplex.setParam(IloCplex.IntParam.ParamDisplay, false);
            cplex.setParam(IloCplex.DoubleParam.PolishAfterTime, _cplex_timeout - 2);
            cplex.setParam(IloCplex.Param.TimeLimit, _cplex_timeout);
            cplex.setParam(IloCplex.IntParam.MIPDisplay, 0);

            for (final int i : CustomersAndDepots) {
                for (final int j : CustomersAndDepots) {
                    if (i == j) {
                        dist[i][j] = 0;
                        time[i][j] = 0;
                    } else {
                        dist[i][j]  = this.world_map.FWMatrixFastestDist[L2Gid[i]][L2Gid[j]];
                        time[i][j]  = this.world_map.FWMatrixFastestTime[L2Gid[i]][L2Gid[j]];
                    }
                }
            }

            // VARIABLES

            final IloIntVar[][][] x = new IloIntVar[v][][];
            for (final int k : transporters) {
                x[k] = new IloIntVar[CustomersAndDepots.length][];
                for (final int i : CustomersAndDepots) {
                    x[k][i] = new IloIntVar[CustomersAndDepots.length];
                    for(final int j : CustomersAndDepots){
                        x[k][i][j] = cplex.boolVar();           // x[k][i][j] = 1 if transp. k goes from cust. i to j, else 0
                    }
                }
            }

            final IloIntVar[][] p = new IloIntVar[v][];
            for(final int k : transporters){
                p[k] = new IloIntVar[CustomersAndDepots.length];
                for(final int i : CustomersAndDepots){
                    p[k][i] = cplex.intVar(0, Capacity);      // p[k][i] = amount of palettes transp. k unloads at cust. i
                }
            }

            final IloIntVar[][] s = new IloIntVar[v][];
            for(final int k : transporters){
                s[k] = new IloIntVar[CustomersAndDepots.length];
                for(final int i : CustomersAndDepots){
                    s[k][i] = cplex.intVar(0, Time.DAY_LENGTH);  // s[k][i] = time of arrival of transp. k at cust. i
                }
            }

            // OBJECTIVE

            /* profit = (selling cost of 1 palette) * (total palettes delivered) - (cost of 1 km of travel) * (amount of
                        km traveled in total) - (cost of 1 second of driver work) * (the time the driver's transporter
                        arrives back at the depot)*/
            final IloLinearNumExpr profit = cplex.linearNumExpr();
            for(final int k : transporters){
                for(final int i : CustomersAndDepots){
                    profit.addTerm(_cppd, p[k][i]);
                    for(final int j : CustomersAndDepots){
                        profit.addTerm(-_cpkmt *dist[i][j] , x[k][i][j]);
                    }
                }
                profit.addTerm(-_cpst,s[k][CustomersAndDepots.length-1]);
            }

            cplex.addMaximize(profit);

            // CONSTRAINTS

            // no path from i to i
            for(final int k : transporters){
                for(final int i : CustomersAndDepots){
                    cplex.addEq(x[k][i][i], 0, "c1");
                }
            }

            // each visited customer demand fulfilled
            for(final int i : Customers){
                final IloLinearNumExpr c2_1 = cplex.linearNumExpr();
                for(final int k : transporters){
                    for(final int j : CustomersAndDepots){
                        c2_1.addTerm(1, x[k][i][j]);
                    }

                    /* if k unloads some palettes at i, it must also leave i */
                    cplex.add(cplex.ifThen(cplex.ge(p[k][i], 1), cplex.eq(cplex.sum(x[k][i]), 1)));
                    /* if k leaves i, it must unload some palettes there */
                    cplex.add(cplex.ifThen(cplex.eq(cplex.sum(x[k][i]), 1), cplex.ge(p[k][i], 1)));
                }

                final IloLinearNumExpr c2_2 = cplex.linearNumExpr();
                for(final int k : transporters){
                    c2_2.addTerm(1, p[k][i]);
                }

                /* if i was visited by >1 transporter, i must've received the amount of palettes equal to i's demand */
                cplex.add(cplex.ifThen(cplex.ge(c2_1, 1), cplex.eq(c2_2, Demand[i]), "c2_1"));
                /* if i was visited by no transporter, the amount of palettes unloaded at i must equal 0*/
                cplex.add(cplex.ifThen(cplex.eq(c2_1, 0), cplex.eq(c2_2, 0), "c2_2"));
            }

            // no palettes can be unloaded at depot
            for(final int k : transporters){
                cplex.addEq(p[k][0], 0);
                cplex.addEq(p[k][CustomersAndDepots.length -1], 0);
            }

            // capacity of a transporter is observed
            for(final int k : transporters){
                final IloLinearNumExpr c3 = cplex.linearNumExpr();
                for(final int i : Customers){
                    c3.addTerm(1, p[k][i]);
                }
                cplex.addLe(c3, Capacity, "c3");
            }

            // all transporters are leaving depot (0) & entering depot (n + 1)
            for(final int k : transporters) {
                final IloLinearNumExpr c4 = cplex.linearNumExpr();
                for(final int j : CustomersAndDepots){
                    c4.addTerm(1, x[k][0][j]);
                }
                cplex.addEq(c4, 1, "c4");

                final IloLinearNumExpr c6 = cplex.linearNumExpr();
                for(final int i : CustomersAndDepots){
                    c6.addTerm(1, x[k][i][CustomersAndDepots.length - 1]);
                }
                cplex.addEq(c6, 1, "c6");
            }

            // entering a city by a transporter k implies leaving the city by that transporter k
            for(final int h : Customers){
                for(final int k : transporters){
                    final IloLinearNumExpr c5 = cplex.linearNumExpr();
                    for(final int i : CustomersAndDepots){
                        c5.addTerm(1,x[k][i][h]);
                    }
                    for(final int j : CustomersAndDepots){
                        c5.addTerm(-1,x[k][h][j]);
                    }
                    cplex.addEq(c5, 0, "c5");
                }
            }

            // entering a city implies doing so within time window bounds
            for(final int i : CustomersAndDepots){
                for(final int k : transporters){
                    final IloLinearNumExpr c7 = cplex.linearNumExpr();
                    c7.addTerm(1, s[k][i]);
                    cplex.addGe(c7, LBTW[i], "c7_1");
                    cplex.addLe(c7, UBTW[i], "c7_2");
                }
            }

            // number of transporters departing from depot is leq the max. amount of transporters (v)
            final IloLinearNumExpr c8 = cplex.linearNumExpr();
            for(final int k : transporters){
                for(final int j : CustomersAndDepots){
                    c8.addTerm(1,x[k][0][j]);
                }
            }
            cplex.addLe(c8, v, "c8");

            // causality preserved, i.e. transporter k travelling from i to j must first arrive at i, then at j, and
            // it must take it at least the amount of time necessary to travel from i to j
            for(final int k : transporters){
                for(final int i : CustomersAndDepots){
                    for(final int j : CustomersAndDepots){
                        final IloLinearNumExpr c9 = cplex.linearNumExpr();
                        c9.addTerm(1, s[k][i]);
                        c9.addTerm(Transporter.UNLOADING_TIME, p[k][i]);
                        c9.addTerm(-1, s[k][j]);
                        cplex.add(
                                cplex.ifThen(
                                        cplex.eq(x[k][i][j], 1),
                                        cplex.le(c9, -time[i][j]))
                        );
                    }
                }
            }

            if(cplex.solve()){
                final Set<Integer> accepted_orders = new HashSet<>();

                for(final int k : transporters){
                    final ArrayList<Integer> visited = new ArrayList<>();
                    visited.add(0);
                    int pos = 0;
                    outer:
                    while(pos != CustomersAndDepots.length - 1){
                        for(final int j : CustomersAndDepots){
                            final double val = cplex.getValue(x[k][pos][j]);
                            if(doubleEquals(val, 1.0)){
                                pos = j;
                                visited.add(pos);
                                if(pos != CustomersAndDepots.length - 1) {
                                    accepted_orders.add(pos - 1);
                                }
                                continue outer;
                            }
                        }
                    }

                    if(visited.size() > 2){                         // if transporter doesn't stay in the depot
                        final int bound = visited.size() - 1;

                        final Edge[][] paths = new Edge[bound][];
                        final int[] unloading_amounts = new int[bound - 1];
                        final int[] waiting_times = new int[bound - 1];

                        paths[0] = this.world_map.fastestPath(L2Gid[visited.get(0)], L2Gid[visited.get(1)]);

                        int lastS = 0;
                        int prevId = visited.get(0);
                        int id = visited.get(1);
                        int unloading_time_sum = 0;

                        int unloading_amounts_sum = 0;
                        int time_sum = 0;
                        int path_length_total = 0;

                        for(int i = 1; i < bound; i++){
                            final int nextId = visited.get(i + 1);

                            final int a = L2Gid[id];
                            final int b = L2Gid[nextId];
                            paths[i] = this.world_map.fastestPath(a, b);
                            path_length_total += this.world_map.fastestPathDist(a, b);
                            time_sum += this.world_map.fastestPathTime(a, b);

                            final int unloading_amount = Math.toIntExact(Math.round(cplex.getValue(p[k][id])));
                            unloading_amounts[i - 1] = unloading_amount;
                            unloading_amounts_sum += unloading_amount;

                            final int newS =(int) cplex.getValue(s[k][id]);
                            waiting_times[i - 1] = (int) (newS - lastS - time[prevId][id]) - unloading_time_sum;
                            time_sum += waiting_times[i - 1];

                            unloading_time_sum += unloading_amount * Transporter.UNLOADING_TIME;

                            lastS = newS;
                            prevId = id;
                            id = nextId;
                        }

                        final Transporter nt = new Transporter(this, paths, unloading_amounts, waiting_times, departure_time);
                        this.assigned_transporters.add(nt);

                        final int current_day = Time.current_day();

                        this.delivered_palettes_in_day[current_day] += unloading_amounts_sum;
                        this.km_travelled_in_day[current_day] += path_length_total;
                        this.profit_in_day[current_day] += cplex.getObjValue();

                        this.palettes_delivered_by_transporter_in_day[nt.getId()][current_day] += unloading_amounts_sum;
                        this.km_travelled_by_transporter_in_day[nt.getId()][current_day] += path_length_total;
                        this.s_travelled_by_transporter_in_day[nt.getId()][current_day] += time_sum;

                        this.palettes_delivered_by_transporter_total[nt.getId()] += unloading_amounts_sum;
                        this.km_travelled_by_transporter_total[nt.getId()] += path_length_total;
                        this.s_travelled_by_transporter_total[nt.getId()] += time_sum;
                    }
                }

                for(int i = 0; i < orders.size(); i++){
                    if(!accepted_orders.contains(i)){
                        orders.get(i).decline();
                    }
                    else {
                        acceptOrder(orders.get(i));
                    }
                }

            }else{
                orders.forEach(Order::decline);
            }

        }catch (final IloException e){
            e.printStackTrace();
        }
    }

    /**
     * Prepares an order for dispatching before the deliveries begin.
     * Declines the order if it is unreachable in time.
     * @param order target
     */
    public void prepareOrder(final Order order) {
        if(establishRoute(this.company_location, DELIVERY_START_TIME, order) >= 0) {
            this.orders.add(order);
            this.orderGrid[order.getGridX()][order.getGridY()].add(order);
        }else{
            order.decline();
        }
    }

    /**
     * Prepares an order for dispatching during the day.
     * Declines the order if it is unreachable in time.
     * @param order target
     */
    public void processOrder(final Order order) {
        int time = establishRoute(this.company_location, Time.current_time(), order);
        if(time >= 0){
            time += Time.current_time();
            this.orders.add(order);
            this.orderGrid[order.getGridX()][order.getGridY()].add(order);
            if(this.recalculation_time > time) this.recalculation_time = time;
        }else{
            order.decline();
        }
    }

    /**
     * Calls {@link Company#solveOrders()} at the last possible moment when
     * all orders prepared by {@link Company#processOrder(Order)} are still reachable in time.
     */
    public void recalculate() {
        if(Time.current_time() >= this.recalculation_time){
            toggleLoading();
            statusMessage("Calculating the best routes for transporters to maximize profit. (done 0/" + this.orders.size() + ")");

            solveOrders();

            toggleLoading();
            statusMessage("Ready");
        }
    }

    private void resetRCTime() {
        this.recalculation_time = Time.DAY_LENGTH * (Time.current_day() + 1) + 100;
    }

    private void acceptOrder(final Order order) {
        this.settlement_orderAmounts[order.getLocation()] += order.getSize();
        order.accept();
    }

    /**
     * Called by transporters beginning to unload palettes at a target location.
     * @param id target location
     */
    void informUnloadingBegin(final int id) {
        this.settlement_transporters[id]++;
        Main.updateCustomerInTable(id);
    }

    /**
     * Called by transporters having completed their unloading at a target location.
     * @param id target location
     * @param unloading_amount amount of palettes unloaded there
     */
    void informUnloadingEnd(final int id, final int unloading_amount) {
        this.settlement_transporters[id]--;
        this.settlement_orderAmounts[id] -= unloading_amount;
        Main.updateCustomerInTable(id);
    }

    /**
     * For a target customer, returns "ACCEPTING DELIVERY(n)" where n is the amount of transporters currently unloading
     * at this customer's location, if there are none, returns "IDLE".
     * @param id target location
     * @return "ACCEPTING DELIVERY(n)" / "IDLE"
     */
    public String customerState(final int id) {
        if(this.settlement_transporters[id] > 0) {
            return ACCEPTING_STATE + " (" + this.settlement_transporters[id] + ")";
        }else{
            return IDLE_STATE;
        }
    }

    /**
     * Exports the delivery statistics to a .CSV file.
     * @throws IOException output error
     */
    public void exportStatistics() throws IOException {
        final BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(Main.STATISTICS_CSV));

        final int dayCount = Main.INIT_ORDERS.length;
        bos.write("General stats:\n".getBytes());

        StringBuilder sb = new StringBuilder(";");
        for(int i = 0; i < dayCount; i++){
            sb.append("Day ").append(i).append(";");
        }
        sb.append("\n");
        bos.write(sb.toString().getBytes());
        sb = new StringBuilder("Palettes delivered (n);");
        for(int i = 0; i < dayCount; i++) {
            sb.append(this.delivered_palettes_in_day[i]).append(";");
        }
        sb.append("\n");
        bos.write(sb.toString().getBytes());

        sb = new StringBuilder("Length travelled (km);");
        for(int i = 0; i < dayCount; i++) {
            sb.append(this.km_travelled_in_day[i]).append(";");
        }
        sb.append("\n");
        bos.write(sb.toString().getBytes());

        sb = new StringBuilder("Profit (PU);");
        for(int i = 0; i < dayCount; i++) {
            sb.append(String.format("%.2f;", this.profit_in_day[i]));
        }
        sb.append("\n\n");
        bos.write(sb.toString().getBytes());

        bos.write("Delivered palettes by individual transporters:\n".getBytes());

        sb = new StringBuilder("Transporter ID;");
        for(int i = 0; i < dayCount; i++) {
            sb.append("Day ").append(i).append(";");
        }
        sb.append("Total;\n");
        bos.write(sb.toString().getBytes());

        for(int k = 0; k < Transporter.highestIndexUsed; k++){
            sb = new StringBuilder(String.valueOf(k)).append(";");
            for(int i = 0; i < dayCount; i++) {
                sb.append(this.palettes_delivered_by_transporter_in_day[k][i]).append(";");
            }
            sb.append(this.palettes_delivered_by_transporter_total[k]);
            sb.append("\n");
            bos.write(sb.toString().getBytes());
        }
        bos.write("\n".getBytes());

        bos.write("Kilometers traveled by individual transporters:\n".getBytes());

        sb = new StringBuilder("Transporter ID;");
        for(int i = 0; i < dayCount; i++) {
            sb.append("Day ").append(i).append(";");
        }
        sb.append("Total;\n");
        bos.write(sb.toString().getBytes());

        for(int k = 0; k < Transporter.highestIndexUsed; k++){
            sb = new StringBuilder(String.valueOf(k)).append(";");
            for(int i = 0; i < dayCount; i++) {
                sb.append(this.km_travelled_by_transporter_in_day[k][i]).append(";");
            }
            sb.append(this.km_travelled_by_transporter_total[k]);
            sb.append("\n");
            bos.write(sb.toString().getBytes());
        }
        bos.write("\n".getBytes());

        bos.write("Seconds traveled by individual transporters:\n".getBytes());

        sb = new StringBuilder("Transporter ID;");
        for(int i = 0; i < dayCount; i++) {
            sb.append("Day ").append(i).append(";");
        }
        sb.append("Total;\n");
        bos.write(sb.toString().getBytes());

        for(int k = 0; k < Transporter.highestIndexUsed; k++){
            sb = new StringBuilder(String.valueOf(k)).append(";");
            for(int i = 0; i < dayCount; i++) {
                sb.append(this.s_travelled_by_transporter_in_day[k][i]).append(";");
            }
            sb.append(this.s_travelled_by_transporter_total[k]);
            sb.append("\n");
            bos.write(sb.toString().getBytes());
        }

        bos.close();
    }
}
