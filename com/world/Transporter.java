package com.world;

import com.graph.Edge;
import com.misc.LLFunction;

import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * Represents a single transporter used to traverse the country map and deliver palettes to customers.
 */
public class Transporter {
    /**
     * The maximum amount of palettes one can fit inside a single transporter.
     */
    static final int MAXIMUM_CAPACITY = 6;

    /**
     * The possible states of a transporter.
     */
    private static final int DRIVING_STATE = 0;
    private static final int WAITING_STATE = 1;
    private static final int UNLOADING_STATE = 2;

    /**
     * The amount of time in seconds it takes the transporter staff to unload a single palette.
     */
    static final int UNLOADING_TIME = 1800;

    /**
     * Current amount of palettes onboard.
     */
    private int load;

    /**
     * Current road the transporter is traversing.
     */
    private int current_edge;

    /**
     * Current path the transporter is traversing. (path = list(roads))
     */
    private int current_path;

    /**
     * The time the transporter will depart from depot.
     */
    private final int departure_time;

    /**
     * A number between 0 and 1 representing the transporter's progress of the current road in terms of traversing it.
     */
    private double current_edge_position;

    /**
     * A number between 0 and 1 representing the transporter's progress of the current path in terms of traversing it.
     */
    private double current_path_position;

    /**
     * The state in which the transporter currently is.
     */
    private int state;

    /**
     * The paths the transporter will take throughout its journey.
     */
    private final Edge[][] paths;

    /**
     * The times the transporter will take to unload its cargo at each stop.
     */
    private final int[] unloading_times;

    /**
     * An array of functions which alter the transporter's state and move him through time.
     */
    private final LLFunction[] trans_functions;

    /**
     * Available transporters at the company's depot.
     */
    private static final PriorityQueue<Integer> indices = new PriorityQueue<>(2000, Comparator.comparingInt(i -> i));

    /**
     * The largest number of transporters that have been traversing the country at the same time during the simulation.
     */
    static int highestIndexUsed = 0;
    static {
        for(int i = 0; i < 2000; i++){
            indices.add(i);
        }
    }

    /**
     * The transporter's "licence plate".
     */
    private final int id;

    /**
     * Creates a transporter and gives its driver all necessary instructions to completely determine its behavior between
     * its departure from the depot and arrival back at the depot.
     * @param c owner company
     * @param paths paths to use
     * @param unloading_amounts how many palettes to unload at each stop
     * @param waiting_times how much time to wait at each stop
     * @param departure_time when to depart
     */
    Transporter(final Company c, final Edge[][] paths, final int[] unloading_amounts, final int[] waiting_times, final int departure_time) {
        //noinspection ConstantConditions
        this.id = indices.poll();
        if(this.id > highestIndexUsed) {
            highestIndexUsed = this.id;
        }

        this.load = MAXIMUM_CAPACITY;

        this.paths = paths;
        this.unloading_times = new int[unloading_amounts.length];
        for(int i = 0; i < this.unloading_times.length; i++){
            this.unloading_times[i] = unloading_amounts[i] * UNLOADING_TIME;
        }

        this.departure_time = departure_time;

        this.current_edge = 0;
        this.current_path = 0;
        this.current_edge_position = 0.0;

        this.current_path_position = 0.0;

        final int[] path_times = new int[paths.length];
        for(int i = 0; i < path_times.length; i++){
            for(int j = 0; j < paths[i].length; j++){
                path_times[i] += paths[i][j].cost[1];
            }
        }

        this.state = DRIVING_STATE;

        this.trans_functions = new LLFunction[3];

        /* DRIVING STATE */
        this.trans_functions[0] = deltaT -> {
            final Edge[] current_roads = paths[this.current_path];
            final int path_size = current_roads.length;

            final Edge current_road = current_roads[this.current_edge];
            final int edge_time = (int) (current_road.cost[1]);
            final int path_time = path_times[this.current_path];
            final int remaining_edge_time = (int) (current_road.cost[1] * (1 - this.current_edge_position));

            if(deltaT < remaining_edge_time){                       // if the time increment is sufficiently small
                this.current_edge_position += (double)deltaT/edge_time;  // the transporter moves on the edge appropriately
                this.current_path_position += (double)deltaT/path_time;
                deltaT = 0;                                         // and keeps driving.
            }else{                                                  // otherwise,
                this.current_edge_position = 0;                          // the transporter moves to the next edge
                this.current_path_position += (double)(remaining_edge_time)/path_time;

                deltaT -= remaining_edge_time;
                if(++this.current_edge >= path_size){                    // if however there is no next edge
                    this.current_path_position = 0;                      // the transporter moves to the next path
                    if(++this.current_path >= paths.length){             // if however there is no next path,
                        return -1;                                  // the transporter has returned to the depot
                    }

                    this.state = WAITING_STATE;                          // otherwise it has arrived at a customer and is
                                                                    // therefore waiting there
                    this.current_edge = 0;
                }
            }
            return deltaT;
        };

        /* WAITING STATE */
        this.trans_functions[1] = deltaT -> {
            final int waiting_time = waiting_times[this.current_path - 1];
            if(deltaT < waiting_time){                              // if the remaining waiting time is sufficiently large,
                waiting_times[this.current_path - 1] = Math.toIntExact(waiting_time - deltaT);
                deltaT = 0;                                         // the transporter keeps waiting.
            }else{                                                  // otherwise,
                waiting_times[this.current_path - 1] = 0;                // it stops waiting,
                deltaT -= waiting_time;
                this.state = UNLOADING_STATE;                            // and begins to unload its cargo.
                c.informUnloadingBegin(getLastHeading());
            }
            return deltaT;
        };

        /* UNLOADING STATE */
        this.trans_functions[2] = deltaT -> {
            final int unloading_time = this.unloading_times[this.current_path - 1];
            if(deltaT < unloading_time){                            // if the remaining unloading time is sufficiently large,
                this.unloading_times[this.current_path - 1] = Math.toIntExact(unloading_time - deltaT);
                deltaT = 0;                                         // the transporter keeps unloading.
            }else{                                                  // otherwise,
                this.unloading_times[this.current_path - 1] = 0;              // it stops unloading
                this.load -= unloading_amounts[this.current_path - 1];
                c.informUnloadingEnd(getLastHeading(), unloading_amounts[this.current_path - 1]);
                deltaT -= unloading_time;
                this.state = DRIVING_STATE;                              // and proceeds to drive to the next customer / depot
            }
            return deltaT;
        };
    }

    /**
     * @return when the transporter departs from depot
     */
    int getDepartureTime() {
        return this.departure_time;
    }

    private int getCurrentHeading() {
        return this.paths[this.current_path][this.paths[this.current_path].length-1].to;
    }
    private int getLastHeading() {
        return this.paths[this.current_path][0].from;
    }

    /**
     * Moves the transporter through time using its trans functions. Repeatedly applies Δt to the trans functions until
     * Δt is zero, resulting in change of the transporter's state.
     * @param deltaT Δt
     * @return true if this causes the transporter to return to the depot, otherwise false
     */
    boolean move(long deltaT) {
        while(deltaT > 0){
            deltaT = this.trans_functions[this.state].apply(deltaT);
            if(deltaT == -1) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return A number between 0 and 1 representing the transporter's progress of the current path in terms of traversing it.
     */
    public double pathProgress(){
        return this.current_path_position;
    }

    /**
     * @return Number of palettes currently onboard.
     */
    public int getLoad(){
        return this.load;
    }

    /**
     * @return The current road the transporter is traversing.
     */
    public Edge getCurrentEdge() {
        return this.paths[this.current_path][this.current_edge];
    }

    /**
     * @return A number between 0 and 1 representing the transporter's progress of the current road in terms of traversing it.
     */
    public double edgeProgress() {
        return this.current_edge_position;
    }

    /**
     * @return transporter's unique ID (license plate).
     */
    public int getId() {
        return this.id;
    }

    /**
     * @return the transporter's current path represented by a string of format "A -> B", where A is the path beginning
     * and B is the path end.
     */
    public String getCurrentPath() {
        return getLastHeading() + " -> " + getCurrentHeading();
    }

    /**
     * @return "DRIVING" if the transporter is currently traversing a road, "WAITING" if the transporter is currently
     * parked at a customer waiting to unload, or "UNLOADING" if the transporter is currently unloading palettes.
     */
    public String getState() {
        switch(this.state){
            case DRIVING_STATE: return "DRIVING";
            case WAITING_STATE: return "WAITING";
            case UNLOADING_STATE: return "UNLOADING";
            default: return "null";
        }
    }

    /**
     * Parks the transporter at the depot. Called on transporter arrival to depot.
     * @param id the transporter's id
     */
    static void restoreIndex(final int id){
        Transporter.indices.add(id);
    }
}
