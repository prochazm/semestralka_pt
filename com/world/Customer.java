package com.world;

import java.util.Random;

import static com.main.Main.COMMON_RANDOM;

/**
 * Represents a company's customer.
 */
public class Customer {
    /**
     * Lower bound for accepting-deliveries time window size.
     */
    private static final int MIN_AD_SIZE = Time.toSeconds(1, 0);

    /**
     * Upper bound for accepting-deliveries time window size.
     */
    private static final int MAX_AD_SIZE = Time.toSeconds(2, 0);

    /**
     * Lower bound for accepting-deliveries time window start.
     */
    private static final int AD_SOONEST_START = Time.toSeconds(8, 0);

    /**
     * Upper bound for accepting-deliveries time window end.
     */
    private static final int AD_LATEST_END = Time.toSeconds(20, 0);

    /**
     * An ascendingly-sorted array of 20 integers d from I = {1,2,3,4,5,6} so that forall (i1, i2) in I: |i1| / |i2| = p(i1) / p(i2),
     * where |i| = occurrence count of i in PROB_DISTRO and p(i) is the probability of a customer being of size i.
     */
    private static final int[] PROB_DISTRO = {1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 5, 5, 6};

    /**
     * @return the position of the customer
     */
    public int getPosition() {
        return this.position;
    }

    /**
     * @return the size of the customer
     */
    public int getSize() {
        return this.size;
    }

    /**
     * @return the customer's accepting-deliveries time window start.
     */
    public int getAd_start() {
        return this.ad_start;
    }

    /**
     * @return the customer's accepting-deliveries time window end.
     */
    public int getAd_end() {
        return this.ad_end;
    }

    private final int position, gridX, gridY;
    private final int size;
    private final int ad_start, ad_end;

    /**
     * Creates a customer in the target settlement.
     * @param settlement target
     */
    public Customer(final Settlement settlement) {
        this.position = settlement.id;
        this.gridX = settlement.gridX;
        this.gridY = settlement.gridY;
        final Random r = COMMON_RANDOM;
        final int num = r.nextInt(20);
        this.size = PROB_DISTRO[num];

        final int ad_size = MIN_AD_SIZE + r.nextInt(MAX_AD_SIZE - MIN_AD_SIZE);

        this.ad_start = AD_SOONEST_START + r.nextInt(AD_LATEST_END - AD_SOONEST_START - ad_size);
        this.ad_end = this.ad_start + ad_size;
    }

    /**
     * Creates a new order made by this customer.
     * @return an order
     */
    public Order makeOrder(){
        return new Order(this);
    }

    /**
     * @return the customer's X position in the country agglomerate grid
     */
    public int getGridX() {
        return this.gridX;
    }

    /**
     * @return the customer's Y position in the country agglomerate grid
     */
    public int getGridY() {
        return this.gridY;
    }
}
