package com.world;

import java.io.Serializable;

/**
 * Represents a city in the country map.
 */
public class Settlement implements Serializable {
    public final double x;
    public final double y;
    final int id;
    final int gridX;
    final int gridY;

    /**
     * Founds a new city.
     * @param x x position
     * @param y y position
     * @param gridX x coordinate in the grid
     * @param gridY y coordinate in the grid
     * @param id global id
     */
    public Settlement(final double x, final double y, final int gridX, final int gridY, final int id) {
        this.x = x;
        this.y = y;
        this.id = id;
        this.gridX = gridX;
        this.gridY = gridY;
    }
}
