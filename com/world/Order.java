package com.world;

/**
 * Represents a customer's order of palettes.
 */
public class Order {
    private final int gridX;
    private final int gridY;
    private final int location;
    private final int size;
    private final int ad_start;
    private final int ad_end;

    /**
     * Creates a new order based on the customer's stats.
     * @param customer target customer
     */
    Order(final Customer customer) {
        this.location = customer.getPosition();
        this.size = customer.getSize();
        this.ad_start = customer.getAd_start();
        this.ad_end = customer.getAd_end();
        this.gridX = customer.getGridX();
        this.gridY = customer.getGridY();
    }

    /**
     * @return where should the palettes be delivered to
     */
    int getLocation() {
        return this.location;
    }

    /**
     * @return the amount of palettes that should be delivered
     */
    int getSize() {
        return this.size;
    }

    /**
     * @return time before which the order cannot be delivered
     */
    int getAdStart() {
        return this.ad_start;
    }

    /**
     * @return time after which the order cannot be delivered
     */
    int getAdEnd() {
        return this.ad_end;
    }

    /**
     * Is called when the order is declined by the company.
     */
    @SuppressWarnings("EmptyMethod")
    void decline() {
        //System.out.println("Order " + toString() + " has been DECLINED.");
    }

    /**
     * Is called when the order is accepted by the company.
     */
    @SuppressWarnings("EmptyMethod")
    void accept() {
        //System.out.println("Order " + toString() + " has been ACCEPTED.");
    }

    /**
     * @return the customer's X position in the country agglomerate grid
     */
    int getGridX() {
        return this.gridX;
    }

    /**
     * @return the customer's Y position in the country agglomerate grid
     */
    int getGridY() {
        return this.gridY;
    }
}
