package com.world;

import java.util.Comparator;

/**
 * Represents time in the simulation.
 */
public class Time {
    /**
     * Length of a day in time units (seconds).
     */
    static final int DAY_LENGTH = 86400;

    /**
     * Length of an hour in seconds.
     */
    private static final int HOUR_LENGTH = 3600;

    /**
     * Length of a minute in seconds.
     */
    private static final int MINUTE_LENGTH = 60;

    /**
     * The initial time. The first time. The time zero; then, when it all began. The time at which the concept of time was
     * first thought of. It is not the time when the world began its existence. It is the time at which the time stopped
     * being an abstraction - and became a concrete, shaping and meaningful parameter of life.
     */
    private static final int INIT_TIME = 5 * HOUR_LENGTH;

    /**
     * The current time in seconds.
     */
    private static long total_time = INIT_TIME;

    /**
     * I'm sorry Albert, my Time is absolute.
     */
    private Time() {
    }

    /**
     * Converts time defined by hours and minutes to seconds.
     * @param hours hours
     * @param minutes minutes
     * @return seconds
     */
    public static int toSeconds(final int hours, final int minutes) {
        return hours * HOUR_LENGTH + minutes * MINUTE_LENGTH;
    }

    /**
     * Converts time defined by seconds to a string of format "hh:mm".
     * @param seconds seconds
     * @return hh:mm
     */
    public static String toTime(int seconds) {
        final String format = seconds > 0 ? "%02d:%02d" : "-%02d:%02d";
        seconds = Math.abs(seconds);
        return String.format(format, seconds / HOUR_LENGTH, (seconds % HOUR_LENGTH) / MINUTE_LENGTH);
    }

    /**
     * @return the current day
     */
    public static int current_day() {
        return (int) (total_time / DAY_LENGTH);
    }

    /**
     * @return the current time
     */
    public static int current_time() {
        return (int) (total_time % DAY_LENGTH);
    }

    /**
     * Increments the current time by some Δt.
     * @param increment Δt
     */
    public static void increment_time(final long increment) {
        total_time += increment;
    }

    /**
     * Returns a timestamp of current time in the following format:
     * Day d, hh:mm
     * @return Day d, hh:mm where d is the current day and hh:mm is the current time represented in such format.
     */
    public static String timestamp() {
        return "Day " + Time.current_day() + ", " + Time.toTime(Time.current_time());
    }

    /**
     * Compares two times represented in the "hh:mm" format.
     * The comparator returns 1 if the first time comes after the second one, 0 if they equal, and -1 otherwise.
     */
    public static final Comparator<Object> compareTimes = (o1, o2) -> {
        String[] ss1 = ((String) o1).split(":");
        String[] ss2 = ((String) o2).split(":");
        int a = Time.toSeconds(Integer.parseInt(ss1[0], 10), Integer.parseInt(ss1[1], 10));
        int b = Time.toSeconds(Integer.parseInt(ss2[0], 10), Integer.parseInt(ss2[1], 10));
        return a - b;
    };

    /**
     * @return true if the time has just been initialized, else false
     */
    public static boolean init() {
        return total_time == INIT_TIME;
    }
}
