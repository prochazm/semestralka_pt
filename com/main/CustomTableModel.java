package com.main;

import javax.swing.table.DefaultTableModel;
import java.util.Vector;

/**
 * A DefaultTableModel that is not editable and has the ability
 * to return row id of a target entry determined by its first column value.
 */
class CustomTableModel extends DefaultTableModel {
    /**
     * Creates the table model.
     * @param cnames column names
     * @param i number of rows by default
     */
    CustomTableModel(final String[] cnames, final int i) {
        super(cnames, i);
    }

    /**
     * Tests editability of a target cell.
     * @param row cell row
     * @param column cell column
     * @return false
     */
    @Override
    public boolean isCellEditable(final int row, final int column) {
        return false;
    }

    /**
     * Finds a target table entry by its 'id', which must be in the first column.
     * @param id target it
     * @return row id of the table entry
     */
    int getRowOfId(final int id){
        for(int i = 0; i < this.dataVector.size(); i++){
            final Object probablyID = ((Vector) this.dataVector.get(i)).get(0);
            if(probablyID instanceof Integer && (Integer) probablyID == id){
                return i;
            }
        }
        return -1;
    }
}
