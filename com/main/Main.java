package com.main;

import com.graph.Edge;
import com.graph.GraphGenerator;
import com.graph.ListGraph;
import com.graphics.LabeledSpinner;
import com.misc.Pair;
import com.world.*;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.Timer;

/**
 * Represents the main application's GUI.
 */
public class Main {
    private static final int COMMON_SEED = 100;

    /**
     * Common random object for the whole project.
     */
    public static final Random COMMON_RANDOM = new Random(COMMON_SEED);

    /**
     * Amount of settlements in the country.
     */
    private static final int SETTLEMENT_COUNT = 2000;

    /**
     * Country grid width.
     */
    public static final int GRID_WIDTH = 20;

    /**
     * Country grid height.
     */
    public static final int GRID_HEIGHT = 10;

    /**
     * Amount of customers.
     */
    static final int CUSTOMER_COUNT = 1000;

    /**
     * The time when the orders from customers start coming with exponential probability of T = 300s.
     */
    static final int ORDERS_START = Time.toSeconds(6, 0);

    /**
     * The time when the orders from customers stop coming.
     */
    static final int ORDERS_STOP = Time.toSeconds(16, 0);

    /**
     * The times when customers make orders during the day.
     */
    static Deque<Integer> during_day_orders;

    /**
     * The orders known to the company at the beginning of the day, before the orders are being distributed.
     */
    public static final int[] INIT_ORDERS = {50, 150, 300};

    /**
     * Simulation tics per second.
     */
    private static double _tps = 15.0;

    /**
     * Simulation milliseconds per tick.
     */
    private static double _mspt = 1000 / _tps;

    /**
     * Simulation seconds per tick.
     */
    static double _spt = 1 / _tps;

    /**
     * For each time unit that passes in real world, _rl2sim_factor time units pass in the simulation world.
     */
    static int _rl2sim_factor = 900;

    /**
     * The file where the simulation data is stored.
     */
    private static final String DATA_SIM = "./data.sim";

    /**
     * The file where the statistics are stored.
     */
    public static final String STATISTICS_CSV = "./statistics.csv";

    /**
     * The application's status bar.
     */
    private static Label statusBar;

    /**
     * The company distributing the palettes to the whole country, Mr. Paleta, Syn & Vnuci.
     */
    static Company company;

    /**
     * The company's customers.
     */
    static List<Customer> customers;

    /**
     * Shows current simulation time.
     */
    static Label time;

    /**
     * Shows current assigned/departed transporter counts.
     */
    static Label transporters;

    /**
     * Visualizes the departed transporter states.
     */
    private static JTable customer_table;

    /**
     * Visualizes the customers' states.
     */
    private static JTable transporter_table;

    /**
     * Keeps track of departed transporters
     */
    static Set<Transporter> departedTransporters;

    /**
     * Country cities.
     */
    private static Settlement[] settlements;

    /**
     * A loading gif to calm the user during all computationally-expensive parts of the program.
     */
    private static JLabel loading;

    /**
     * Maps customer id to customer position in {@code customers}.
     */
    private static int[] customer_array;

    /**
     * True if the simulation is currently running, else false.
     */
    private static boolean simulation_running;

    /**
     * Simulation timer.
     */
    private static Timer t;

    /**
     * The application window.
     */
    private static JFrame window;

    /**
     * All settings' spinners.
     */
    private static List<LabeledSpinner> spinners;

    /**
     * Starts and pauses the simulation.
     */
    private static JButton toggle;

    /**
     * Initializes the application, forms the GUI and sets the simulation up.
     * @param args ignored
     */
    public static void main(final String[] args) {
        /* MAIN WINDOW SETUP */
        window = new JFrame("Vehicle routing");
        window.setLayout(new BorderLayout());
        window.setPreferredSize(new Dimension(800, 600));
        window.setMinimumSize(new Dimension(800, 650));

        /* MAIN WINDOW SEPARATE OBJECTS */
        final JComponent buttonPane = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        buttonPane.setPreferredSize(new Dimension(800, 45));
        time = new Label("Time: " + Time.timestamp());
        transporters = new Label("Transporters: 0/0");
        toggle = new JButton("Start/Stop");
        toggle.setEnabled(false);
        loading = new JLabel(new ImageIcon("images/loading_35.gif"));
        loading.setVisible(false);
        loading.setBounds(380, 0, 35, 35);
        window.add(loading);
        buttonPane.add(time);
        buttonPane.add(transporters);
        buttonPane.add(toggle);

        /* MAIN WINDOW LAYOUT */
        final JTabbedPane tabbedPane = new JTabbedPane();
        statusBar = new Label("Ready");
        statusBar.setPreferredSize(new Dimension(800, 15));

        /* STATE TAB LAYOUT */
        final JComponent state = new JPanel(new BorderLayout());
        tabbedPane.addTab("State", null, state,
                "Shows the state of the simulation");

        final JTabbedPane transporters_customers = new JTabbedPane();
        transporters_customers.setPreferredSize(new Dimension(650, 600));
        final JPanel transporter_panel = new JPanel();
        final JPanel customer_panel = new JPanel();
        transporters_customers.addTab("Transporters", null, transporter_panel);
        transporters_customers.addTab("Customers", null, customer_panel);

        state.add(transporters_customers, BorderLayout.CENTER);

        /* INPUT TAB LAYOUT */
        final JComponent input = new JPanel();
        input.setLayout(new BoxLayout(input, BoxLayout.Y_AXIS));
        tabbedPane.addTab("Input", null, input,
                "Allows declaration of custom orders");

        /* SETTINGS TAB LAYOUT */
        final JComponent settings = new JPanel();
        settings.setLayout(new BoxLayout(settings, BoxLayout.Y_AXIS));
        tabbedPane.addTab("Settings", null, settings);
        tabbedPane.setSelectedComponent(settings);

        /* MAIN WINDOW INIT */
        window.add(tabbedPane, BorderLayout.CENTER);
        window.add(statusBar, BorderLayout.PAGE_END);
        window.add(buttonPane, BorderLayout.PAGE_START);

        window.pack();
        window.setTitle("Vehicle routing");
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        window.setLocationRelativeTo(null);
        window.setVisible(true);

        /* INPUT ITEMS */
        final JPanel cocontainer = new JPanel();
        cocontainer.setLayout(new FlowLayout(FlowLayout.CENTER));
        cocontainer.setPreferredSize(new Dimension(400, 40));
        final LabeledSpinner customOrder = new LabeledSpinner(0, 0, 2000, 1, "Customer id: ");
        final JButton makeCustomOrder = new JButton("Order");
        makeCustomOrder.setEnabled(false);

        cocontainer.add(customOrder);
        cocontainer.add(makeCustomOrder);
        customOrder.setDisabled();
        input.add(cocontainer);

        /* SETTINGS ITEMS */
        spinners = new ArrayList<>();
        final LabeledSpinner rl2sim_factor = new LabeledSpinner(_rl2sim_factor, 1, 9999, 1, "Real life to simulaton factor: ");
        final LabeledSpinner tps = new LabeledSpinner(_tps, 0.016, 9999.0, 0.1, "Tics per second: ");
        final LabeledSpinner cpkmt = new LabeledSpinner(Company._cpkmt, 0.0, 9999.0, 1.0, "Cost (of gasoline) per kilometer of travel: ");
        final LabeledSpinner cpst = new LabeledSpinner(Company._cpst, 0.0, 99, 0.01, "Cost (of a single driver) per second of travel: ");
        final LabeledSpinner cppd = new LabeledSpinner(Company._cppd, 0.0, 999999.0, 1.0, "Cost of a single palette delivered: ");
        final LabeledSpinner cluster_bound = new LabeledSpinner(Company._cluster_size_bound, 2, 9999, 1, "Max. cluster size (exclusive): ");
        final LabeledSpinner cplex_timeout = new LabeledSpinner(Company._cplex_timeout, 0, 9999, 1, "Max. time spent solving 1 cluster: ");

        rl2sim_factor.addChangeListener(e -> _rl2sim_factor = (int) rl2sim_factor.getValue());
        tps.addChangeListener(e -> {
            _tps = (double) tps.getValue();
            _mspt = 1000 / _tps;
            _spt = 1 / _tps;
        });
        cpkmt.addChangeListener(e -> Company._cpkmt = (double) cpkmt.getValue());
        cpst.addChangeListener(e -> Company._cpst = (double) cpst.getValue());
        cppd.addChangeListener(e -> Company._cppd = (double) cppd.getValue());
        cluster_bound.addChangeListener(e -> Company._cluster_size_bound = (int) cluster_bound.getValue());
        cplex_timeout.addChangeListener(e -> Company._cplex_timeout = (int) cplex_timeout.getValue());

        spinners.add(rl2sim_factor);
        spinners.add(tps);
        spinners.add(cpkmt);
        spinners.add(cpst);
        spinners.add(cppd);
        spinners.add(cluster_bound);
        spinners.add(cplex_timeout);

        spinners.forEach(settings::add);

        final JButton confirm = new JButton("Confirm");
        confirm.addActionListener(e -> {
            spinners.forEach(LabeledSpinner::setDisabled);
            confirm.setEnabled(false);
            toggle.setEnabled(true);
            tabbedPane.setSelectedIndex(0);
        });
        confirm.setEnabled(false);
        settings.add(confirm);

        /* STATE ITEMS */
        final Dimension table_size = new Dimension(640, 440);
        final Dimension psvs = new Dimension(640, 480);
        final Dimension tsps = new Dimension(658, 440);

        /* CUSTOMER TABLE */
        final String[] customer_table_column_names = {"ID", "Agglomerate", "Size", "AD start", "AD end", "State"};
        customer_table = new JTable(new CustomTableModel(customer_table_column_names, CUSTOMER_COUNT));
        customer_table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        customer_table.setMaximumSize(table_size);
        customer_table.setMinimumSize(table_size);

        customer_table.getColumnModel().getColumn(0).setPreferredWidth(60);
        customer_table.getColumnModel().getColumn(1).setPreferredWidth(110);
        customer_table.getColumnModel().getColumn(2).setPreferredWidth(50);
        customer_table.getColumnModel().getColumn(3).setPreferredWidth(100);
        customer_table.getColumnModel().getColumn(4).setPreferredWidth(100);
        customer_table.getColumnModel().getColumn(5).setPreferredWidth(220);

        final DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        centerRenderer.setVerticalAlignment(JLabel.CENTER);
        customer_table.setDefaultRenderer(Object.class, centerRenderer);
        customer_table.setPreferredScrollableViewportSize(psvs);
        customer_table.setFillsViewportHeight(true);
        final JScrollPane customers_table_scrollpane = new JScrollPane(customer_table);
        customers_table_scrollpane.setPreferredSize(tsps);
        customer_panel.add(customers_table_scrollpane);

        final TableRowSorter<TableModel> sorter = new TableRowSorter<>(customer_table.getModel());
        customer_table.setRowSorter(sorter);
        sorter.setComparator(0, Comparator.comparingInt(o -> (int) o));
        sorter.setComparator(1, (o1, o2) -> {
            final String[] ss1 = ((String) o1).split(",");
            final String[] ss2 = ((String) o2).split(",");
            int a = Integer.parseInt(ss1[0]);
            int b = Integer.parseInt(ss2[0]);
            if(a == b){
                a = Integer.parseInt(ss1[1]);
                b = Integer.parseInt(ss2[1]);
            }
            return a - b;
        });
        sorter.setComparator(2, Comparator.comparingInt(o -> (int) o));
        sorter.setComparator(3, Time.compareTimes);
        sorter.setComparator(4, Time.compareTimes);

        /* TRANSPORTER TABLE */
        final String[] transporter_table_column_names = {"ID", "Current path", "Progress", "Current edge", "Progress", "Coordinates", "Load", "State"};
        transporter_table = new JTable(new CustomTableModel(transporter_table_column_names, 0));
        transporter_table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        transporter_table.setMaximumSize(table_size);
        transporter_table.setMinimumSize(table_size);

        transporter_table.getColumnModel().getColumn(0).setPreferredWidth(40);
        transporter_table.getColumnModel().getColumn(1).setPreferredWidth(100);
        transporter_table.getColumnModel().getColumn(2).setPreferredWidth(80);
        transporter_table.getColumnModel().getColumn(3).setPreferredWidth(100);
        transporter_table.getColumnModel().getColumn(4).setPreferredWidth(80);
        transporter_table.getColumnModel().getColumn(5).setPreferredWidth(100);
        transporter_table.getColumnModel().getColumn(6).setPreferredWidth(60);
        transporter_table.getColumnModel().getColumn(7).setPreferredWidth(80);

        transporter_table.setDefaultRenderer(Object.class, centerRenderer);

        transporter_table.setPreferredScrollableViewportSize(psvs);
        transporter_table.setFillsViewportHeight(true);
        final JScrollPane transporters_table_scrollpane = new JScrollPane(transporter_table);
        transporters_table_scrollpane.setPreferredSize(tsps);
        transporter_panel.add(transporters_table_scrollpane);

        /* FINDING/GENERATING DATA.SIM */
        showMessage("Welcome to the Vehicle Routing simulator. The program will try to " +
                "open "+ DATA_SIM+".\nIf present, the simulation will use it, otherwise a new " +
                "simulation will be generated.");

        toggleLoading();
        statusMessage("Importing " + DATA_SIM);
        Pair<ListGraph, Settlement[]> data = GraphGenerator.importFromFile(DATA_SIM);
        toggleLoading();
        statusMessage("Failed to import " + DATA_SIM);

        if (Objects.isNull(data)){
            showMessage("The file was either not found or corrupted, therefore a new " +
                    "simulation will be generated and stored at " + DATA_SIM);
            toggleLoading();
            statusMessage("Generating the map...");
            data = GraphGenerator.byGrid(GRID_WIDTH, GRID_HEIGHT, 20, SETTLEMENT_COUNT);
            statusMessage("Exporting the generated data to " + DATA_SIM + " ...");
            GraphGenerator.exportToFile(data, DATA_SIM);

            toggleLoading();
            statusMessage("Ready");
            showMessage("The simulation file has been generated and saved successfully.");
        }else{
            statusMessage("Ready");
            showMessage("The file was found and opened successfully.");
        }

        final ListGraph g = data.getA();
        for(int i = 0; i < 2000; i++){
            for(int j = 0; j < 2000; j++){
                final Edge[] path = g.fastestPath(i, j);
                for(int k = 0; k < path.length; k++){
                    if(path[k] == null) {
                        System.out.printf("path %d %d %d is null\n", i, j, k);
                    }
                }
            }
        }

        /* SIMULATION LOADED, SETTINGS ADJUSTABLE */
        spinners.forEach(LabeledSpinner::setEnabled);
        confirm.setEnabled(true);
        customOrder.setEnabled();

        settlements = data.getB();
        company = new Company(data);

        final ArrayList<Integer> indices = new ArrayList<>();

        final int company_id = company.getCompany_location();

        for(int i = 0; i < company_id; i++){
            indices.add(i);
        }
        for(int i = company_id + 1; i < SETTLEMENT_COUNT; i++){
            indices.add(i);
        }
        for(int i = 0; i < SETTLEMENT_COUNT-1; i++){
            final int a = COMMON_RANDOM.nextInt(SETTLEMENT_COUNT-1);
            final int b = (a + COMMON_RANDOM.nextInt(SETTLEMENT_COUNT - 2) + 1) % (SETTLEMENT_COUNT - 1);
            Collections.swap(indices, a, b);
        }

        customers = new ArrayList<>();

        for(int i = 0; i < CUSTOMER_COUNT; i++) {
            customers.add(new Customer(data.getB()[indices.get(i)]));
        }
        customer_array = new int[SETTLEMENT_COUNT];
        Arrays.fill(customer_array, -1);
        for(int i = 0; i < CUSTOMER_COUNT; i++){
            customer_array[customers.get(i).getPosition()] = i;
        }
        /* COMPANY AND CUSTOMERS LOADED */

        makeCustomOrder.addActionListener(e -> {
            final int id = (int) customOrder.getValue();
            if(customer_array[id] >= 0){
                company.processOrder(customers.get(customer_array[id]).makeOrder());
                statusMessage("Customer " + id + "'s order has been passed for processing.");
            }else{
                statusErrorMessage(id + " is not a legit customer ID. Check the Customer table for details.");
            }
        });
        makeCustomOrder.setEnabled(true);
        customOrder.setEnabled();

        for(int i = 0; i < CUSTOMER_COUNT; i++){
            final Customer c = customers.get(i);
            customer_table.setValueAt(c.getPosition(), i, 0);
            customer_table.setValueAt(c.getGridX() + "," + c.getGridY(), i, 1);
            customer_table.setValueAt(c.getSize(), i, 2);
            customer_table.setValueAt(Time.toTime(c.getAd_start()), i, 3);
            customer_table.setValueAt(Time.toTime(c.getAd_end()), i, 4);
            customer_table.setValueAt("IDLE", i, 5);
        }

        /* CUSTOM ORDERS ENABLED, SIMULATION INIT ENABLED */
        statusMessage(String.format("%d customers registered. Press Start/Stop to begin the simulation.\n", CUSTOMER_COUNT));

        departedTransporters = new LinkedHashSet<>();

        toggle.addActionListener( e -> {
            if (simulation_running) {
                t.cancel();
                t.purge();
            } else {
                t = new Timer();
                t.schedule(new Simulation(), 0, (long) _mspt);
            }
            simulation_running = !simulation_running;
        });

        window.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(final WindowEvent e) {
                showMessage("The simulation has ended. The statistics will now be exported to " + Main.STATISTICS_CSV);
                statusMessage("Exporting statistics to " + Main.STATISTICS_CSV);
                toggleLoading();
                try {
                    company.exportStatistics();
                    showMessage("Statistics successfully exported. The program is finished.");
                } catch (final IOException e1) {
                    showMessage("Failed to export statistics.");
                }
                toggleLoading();
            }
        });
    }

    /**
     * Generates initial orders based on the {@link Main#INIT_ORDERS} array.
     * @param init_orders amount of initial orders.
     */
    static void generateInitOrders(final int init_orders) {
        final ArrayList<Integer> indices = new ArrayList<>();
        for(int i = 0; i < init_orders; i++){
            indices.add(i);
        }
        for(int i = 0; i < init_orders; i++){
            final int a = COMMON_RANDOM.nextInt(init_orders);
            final int b = (a + COMMON_RANDOM.nextInt(init_orders - 1) + 1) % init_orders;
            Collections.swap(indices, a, b);
        }

        for(int i = 0; i < init_orders; i++){
            company.prepareOrder(customers.get(indices.get(i)).makeOrder());
        }
    }

    /**
     * Adds a transporter to the transporter table.
     * @param transporter target
     */
    static void addTransporterToTable(final Transporter transporter) {
        final Object[] row = transporterRow(transporter);
        ((DefaultTableModel) transporter_table.getModel()).addRow(row);
    }

    /**
     * Updates a transporter in the transporter table.
     * @param transporter target
     */
    static void updateTransporterInTable(final Transporter transporter){
        final Object[] row = transporterRow(transporter);
        final CustomTableModel model = (CustomTableModel) transporter_table.getModel();
        final int rowId = model.getRowOfId(transporter.getId());
        for(int i = 0; i < row.length; i++){
            transporter_table.setValueAt(row[i], rowId, i);
        }
    }

    /**
     * Removes a transporter from the table.
     * @param transporter target
     */
    static void removeTransporterFromTable(final Transporter transporter){
        final CustomTableModel model = (CustomTableModel) transporter_table.getModel();
        model.removeRow(model.getRowOfId(transporter.getId()));
    }

    /**
     * Updates a customer in the customer table.
     * @param id target customer id
     */
    public static void updateCustomerInTable(final int id){
        final Object[] row = customerRow(customers.get(customer_array[id]));
        final int rowId = customer_table.convertRowIndexToView(customer_array[id]);
        for(int i = 0; i < row.length; i++){
            customer_table.setValueAt(row[i], rowId, i);
        }
    }

    /**
     * Generates customer table entry row for a customer.
     * @param customer target
     * @return data row
     */
    private static Object[] customerRow(final Customer customer){
        return new Object[]{
                customer.getPosition(), customer.getGridX() + "," + customer.getGridY(), customer.getSize(),
                Time.toTime(customer.getAd_start()), Time.toTime(customer.getAd_end()), company.customerState(customer.getPosition())
        };
    }

    /**
     * Generates transporter table entry row for a transporter.
     * @param transporter target
     * @return data row
     */
    private static Object[] transporterRow(final Transporter transporter){
        final Edge current_edge = transporter.getCurrentEdge();
        final double current_edge_progress = transporter.edgeProgress();
        final double x = settlements[current_edge.from].x + (settlements[current_edge.from].x - settlements[current_edge.to].x)*current_edge_progress;
        final double y = settlements[current_edge.from].y + (settlements[current_edge.from].y - settlements[current_edge.to].y)*current_edge_progress;
        final String coords = String.format("[%.1f, %.1f]", x, y);
        return new Object[]{
                transporter.getId(), transporter.getCurrentPath(), String.format("%.0f %%", transporter.pathProgress() * 100), current_edge,
                String.format("%.0f %%", current_edge_progress * 100), coords, transporter.getLoad(), transporter.getState()
        };
    }


    /**
     * Generates a sequence of integers that meet the conditions for an exponential probabilistic sequence.
     * @param start sequence start
     * @param stop sequence end
     * @param expected_value the average value of the interval between two consequent numbers
     * @return the exp. sequence
     */
    @SuppressWarnings("SameParameterValue")
    static Deque<Integer> generateEXPsequence(final int start, final int stop, final int expected_value) {
        final LinkedList<Integer> ret = new LinkedList<>();
        int current = start;
        final double lambda = (double) 1 / expected_value;
        while(true){
            current += (int) EXP(lambda);
            if(current < stop){
                ret.add(current);
            }else {
                break;
            }
        }
        return ret;
    }

    private static double EXP(final double lambda){
        final double rand = COMMON_RANDOM.nextDouble();
        return -Math.log(1 - rand) / lambda;
    }

    /**
     * Displays a message on the status bar.
     * @param s message
     */
    public static void statusMessage(final String s){
        statusBar.setText(s);
    }

    private static void statusErrorMessage(final String s){
        if(statusBar.getForeground() == Color.RED) {
            return;
        }

        final String original = statusBar.getText();
        statusBar.setText(s);
        statusBar.setForeground(Color.RED);
        final TimerTask revert = new TimerTask() {
            @Override
            public void run() {
                statusBar.setText(original);
                statusBar.setForeground(Color.BLACK);
            }
        };
        final Timer t = new Timer();
        t.schedule(revert, 3000);
    }

    /**
     * Toggles the loading gif.
     */
    public static void toggleLoading(){
        loading.setVisible(!loading.isShowing());
    }

    /**
     * Displays a message in a dialog box.
     * @param message message
     */
    static void showMessage(final String message){
        JOptionPane.showMessageDialog(window, message);
    }
}
