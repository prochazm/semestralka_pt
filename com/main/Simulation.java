package com.main;

import com.world.Time;
import com.world.Transporter;

import java.io.IOException;
import java.util.List;
import java.util.TimerTask;

import static com.main.Main.*;

/**
 * Represents a single tick in the simulation.
 */
class Simulation extends TimerTask {
    /**
     * Ticks the simulation. Generates new order data on a new day, during the day it handles transporter departing and
     * moving.
     */
    @Override
    public void run() {
        final int day = Time.init() ? -1 : Time.current_day(); // so #26 gets triggered on simulation init
        Time.increment_time((long) (_rl2sim_factor * _spt));
        final int dayNow = Time.current_day();

        if(dayNow > day){
            toggleLoading();
            statusMessage("Generating a new exponential sequence");
            during_day_orders = generateEXPsequence(ORDERS_START, ORDERS_STOP, 300);
            try {
                final int init_orders = INIT_ORDERS[dayNow];
                statusMessage("Generating new " + init_orders + "initial orders");
                generateInitOrders(init_orders);
                statusMessage("Calculating the best routes for transporters to maximize profit. (done 0/" + init_orders + ")");
                company.solveOrders();
            }catch (final ArrayIndexOutOfBoundsException e){
                toggleLoading();
                showMessage("The simulation has ended. The statistics will now be exported to " + Main.STATISTICS_CSV);
                statusMessage("Exporting statistics to " + Main.STATISTICS_CSV);
                toggleLoading();
                try {
                    company.exportStatistics();
                    showMessage("Statistics successfully exported. The program is finished.");
                } catch (final IOException e1) {
                    showMessage("Failed to export statistics.");
                }
                toggleLoading();
                System.exit(0);
            }
            statusMessage("Ready");
            toggleLoading();
        }else {
            company.recalculate();
        }

        while (!during_day_orders.isEmpty() && Time.current_time() >= during_day_orders.peek()) {
            during_day_orders.poll();
            company.processOrder(customers.get(COMMON_RANDOM.nextInt(CUSTOMER_COUNT)).makeOrder());
        }

        final List<Transporter> arrived = company.moveTransporters((long) (_rl2sim_factor * _spt));
        for (final Transporter transporter : arrived) {
            departedTransporters.remove(transporter);
            removeTransporterFromTable(transporter);
        }

        final List<Transporter> departed = company.departTransporters();
        for (final Transporter transporter : departed) {
            departedTransporters.add(transporter);
            addTransporterToTable(transporter);
        }

        departedTransporters.forEach(Main::updateTransporterInTable);

        time.setText("Time: " + Time.timestamp());
        transporters.setText("Transporters: " + company.atCount() + "/" + company.dtCount());
    }
}
